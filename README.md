# Outlay Expense Monitor

This project was generated with [mastic](https://mastic.gitlab.io/) version 0.2.0.

## Model

```plantuml

class Account {
  + String name
  + String institution
  + String accountNumber
  + Boolean expenseAccount
  + Currency currency
  + Transaction transactions
  + String readerName
}


class Category {
  + String name
  + String colorCode
}


class Transaction {
  + Account account
  + String desription
  + Double amount
  + Currency currency
  + String type
  + Datetime bookedOn
  + Datetime executedOn
  + String explanation
  + String reference
  + String corresponderName
  + String corresponderBank
  + String corresponderAccount
}
Transaction "*" *--o "1" Account : account

class TransactionCategory {
  + Transaction transaction
  + Category category
  + Datetime assignedOn
  + String assignedBy
}
TransactionCategory "*" *--o "1" Transaction : transaction
TransactionCategory "*" *--o "1" Category : category

class InputType {
  + String name
  + String importer
}


class TransactionInput {
  + String content
  + InputType type
  + InputStatus status
  + Long accountId
}
TransactionInput "*" *--o "1" InputType : type

```

[//]: # "mastic-pin-readme"

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
