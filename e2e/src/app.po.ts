import { browser, by, element } from 'protractor';

export class AppPage {
  async navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl);
  }

  async getTitleText(): Promise<string> {
    return new Promise((resolve) => {
      browser.getTitle().then(resolve);
    });
  }

  async getHeaderText(): Promise<string> {
    return element(
      by.css('body > app-root > div > app-header > mat-toolbar > div:nth-child(2) > a')
    ).getText();
  }
}
