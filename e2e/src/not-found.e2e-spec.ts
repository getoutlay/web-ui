import { browser, logging } from 'protractor';
import { NotFoundPage } from './not-found.po';

describe('NotFound Page', () => {
  let page: NotFoundPage;

  beforeEach(() => {
    page = new NotFoundPage();
  });

  it('should display app title', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('Outlay Expense Monitor');
  });

  it('should display app name', () => {
    page.navigateTo();
    expect(page.getHeaderText()).toEqual('Outlay Expense Monitor');
  });

  it('missing page redirects to page not found', () => {
    page.navigateTo();
    expect(page.getBodyText()).toEqual('page not found!');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE,
      } as logging.Entry)
    );
  });
});
