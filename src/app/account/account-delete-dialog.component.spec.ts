import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AccountDeleteDialogComponent } from './account-delete-dialog.component';
import { SharedModule } from '../shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

describe('AccountDeleteDialogComponent', () => {
  let component: AccountDeleteDialogComponent;
  let fixture: ComponentFixture<AccountDeleteDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AccountDeleteDialogComponent],
      imports: [SharedModule, BrowserAnimationsModule],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            selection: [{
              id: '1',
              name: 'Malayan Tapir',
              institution: 'Malayan Tapir',
              accountNumber: 'Malayan Tapir',
              expenseAccount: false,
              currency: 0,
              readerName: 'Malayan Tapir',
            }],
          },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountDeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have data', () => {
    expect(component.data).toBeTruthy();
    expect(component.data.selection).toBeTruthy();
    expect(component.data.selection.length).toEqual(1);
  });

});
