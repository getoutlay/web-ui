import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AccountFormComponent } from './account-form.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { RouterModule } from '@angular/router';

const createNewEvent = (eventName: string, bubbles = false, cancelable = false): CustomEvent => {
  const evt = document.createEvent('CustomEvent');
  evt.initCustomEvent(eventName, bubbles, cancelable, null);
  return evt;
};

describe('AccountFormComponent', () => {
  let component: AccountFormComponent;
  let fixture: ComponentFixture<AccountFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers), RouterModule.forRoot([], { relativeLinkResolution: 'legacy' })],
      declarations: [AccountFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have name', () => {
    expect(component.name).toBeTruthy();
  });

  it('should have institution', () => {
    expect(component.institution).toBeTruthy();
  });

  it('should have accountNumber', () => {
    expect(component.accountNumber).toBeTruthy();
  });

  it('should have expenseAccount', () => {
    expect(component.expenseAccount).toBeTruthy();
  });

  it('should have currency', () => {
    expect(component.currency).toBeTruthy();
  });

  it('should have readerName', () => {
    expect(component.readerName).toBeTruthy();
  });

  it('initially should be invalid', () => {
    expect(component.accountForm.valid).toBeFalsy();
  });

  it('initially should be valid upon input', () => {
    const event = createNewEvent('input');

    const name = fixture.nativeElement.querySelector('#name');
    name.value = 'Malayan Tapir';
    name.dispatchEvent(event);

    const institution = fixture.nativeElement.querySelector('#institution');
    institution.value = 'Malayan Tapir';
    institution.dispatchEvent(event);

    const accountNumber = fixture.nativeElement.querySelector('#accountNumber');
    accountNumber.value = 'Malayan Tapir';
    accountNumber.dispatchEvent(event);

    const expenseAccount = fixture.nativeElement.querySelector('#expenseAccount');
    expenseAccount.value = false;
    expenseAccount.dispatchEvent(event);

    component.currency.setValue(0);
    component.readerName.setValue('test');

    const readerName = fixture.nativeElement.querySelector('#readerName');
    readerName.value = 'Malayan Tapir';
    readerName.dispatchEvent(event);

    expect(component.accountForm.valid).toBeTruthy();
  });
});
