import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, AbstractControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { State } from '../reducers';
import { accountActionTypes } from './account.actions';
import { Observable } from 'rxjs';
import { Account } from './account.model';
import { Router } from '@angular/router';
import { InputType } from '../input-type/input-type.model';
import { inputTypeActionTypes } from '../input-type/input-type.actions';
import { selectAllInputTypes } from '../input-type/input-type.selectors';

@Component({
  selector: 'app-account-form',
  templateUrl: './account-form.component.html',
  styleUrls: ['./account-form.component.sass'],
})
export class AccountFormComponent implements OnInit {
  @Input() account: Observable<Account>;
  readers$: Observable<InputType[]>;

  accountForm = new FormGroup({
    id: new FormControl(''),
    name: new FormControl(''),
    institution: new FormControl(''),
    accountNumber: new FormControl(''),
    expenseAccount: new FormControl(''),
    currency: new FormControl(''),
    readerName: new FormControl(''),
  });

  constructor(private store: Store<State>, private router: Router) {}

  ngOnInit(): void {
    this.store.dispatch(inputTypeActionTypes.loadInputTypes());
    this.readers$ = this.store.select(selectAllInputTypes);
    if (this.account) {
      this.account.subscribe((account) => {
        if (account && account.id) {
          this.accountForm.setValue(account);
        }
      });
    }
  }

  get name(): FormControl {
    return this.accountForm.get('name') as FormControl;
  }

  get institution(): FormControl {
    return this.accountForm.get('institution') as FormControl;
  }

  get accountNumber(): FormControl {
    return this.accountForm.get('accountNumber') as FormControl;
  }

  get expenseAccount(): FormControl {
    return this.accountForm.get('expenseAccount') as FormControl;
  }

  get currency(): FormControl {
    return this.accountForm.get('currency') as FormControl;
  }

  get readerName(): FormControl {
    return this.accountForm.get('readerName') as FormControl;
  }

  onSubmit(): void {
    if (this.accountForm.valid) {
      if (this.accountForm.value.id) {
        this.store.dispatch(
          accountActionTypes.updateAccount({
            update: {
              id: this.accountForm.value.id,
              changes: this.accountForm.value,
            },
          })
        );
        this.router.navigate(['/account']);
      } else {
        const value = this.accountForm.value;
        const { id, ...account } = value;
        this.store.dispatch(accountActionTypes.createAccount({ account }));
      }
    }
  }
}
