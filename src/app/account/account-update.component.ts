import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Account } from './account.model';
import { Store, select } from '@ngrx/store';
import { State } from '../reducers';
import { selectCurrentAccount } from './account.selectors';
import { accountActionTypes } from './account.actions';

@Component({
  selector: 'app-account-update',
  templateUrl: './account-update.component.html',
  styleUrls: ['./account-update.component.sass'],
})
export class AccountUpdateComponent implements OnInit {
  account$: Observable<Account>;

  constructor(private store: Store<State>, private route: ActivatedRoute) {
    this.route.params.pipe(map((params) => params.id)).subscribe((id) => {
      this.store.dispatch(accountActionTypes.loadAccount({ key: id }));
    });
    this.account$ = this.store.select(selectCurrentAccount);
  }

  ngOnInit(): void {}
}
