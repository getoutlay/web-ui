import { createAction, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { Account } from './account.model';

export const loadAccounts = createAction('[Account/API] Load Accounts');
export const accountsLoaded = createAction('[Account/API] Accounts Loaded Successfully',
  props<{ accounts: Account[] }>());

export const loadAccount = createAction('[Account/API] Load Account', props<{ key: string }>());
export const accountLoaded = createAction('[Account/API] Account Loaded Successfully',
  props<{ account: Account }>());

export const createAccount = createAction('[Account/API] Create Account',
  props<{ account: Account }>());

export const deleteAccounts = createAction('[Account/API] Delete Accounts',
  props<{ keys: string[] }>());

export const updateAccount = createAction('[Account/API] Update Account',
  props<{ update: Update<Account> }>());

export const accountActionTypes = {
  loadAccounts,
  accountsLoaded,
  loadAccount,
  accountLoaded,
  createAccount,
  deleteAccounts,
  updateAccount,
};
