import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Account } from './account.model';
import { selectAllAccounts } from './account.selectors';
import { State } from '../reducers';
import { accountActionTypes } from './account.actions';
import * as uuid from 'uuid';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { AccountDeleteDialogComponent } from './account-delete-dialog.component';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.sass'],
})
export class AccountComponent implements OnInit {
  public dataSource: Observable<Account[]>;
  public displayedColumns: string[] = [
    'id',
    'delete',
    'name',
    'institution',
    'accountNumber',
    'expenseAccount',
    'currency',
    'readerName',
  ];
  public selection = new SelectionModel<Account>(true, []);

  constructor(private store: Store<State>, private dialog: MatDialog) {}

  ngOnInit(): void {
    this.store.dispatch(accountActionTypes.loadAccounts());
    this.dataSource = this.store.select(selectAllAccounts);
  }

  onDeleteClick(event: Event): void {
    // prevent parent event happening (navigation)
    event.preventDefault();
    event.stopImmediatePropagation();

    const dialogRef = this.dialog.open(AccountDeleteDialogComponent, {
      data: {
        selection: this.selection.selected,
      },
    });
    dialogRef.afterClosed().subscribe((result: Account[]) => {
      const keys = result.map((entity) => entity.id);
      if (keys && keys.length) {
        this.store.dispatch(accountActionTypes.deleteAccounts({ keys }));
        this.selection.clear();
      }
    });
  }

  toggleSelectAll(): void {
    if (this.selection.hasValue()) {
      this.selection.clear();
    } else {
      this.dataSource.forEach((rows) => rows.forEach((row) => this.selection.select(row)));
    }
  }

  create(entity: Partial<Account>): void {
    const account: Account = {
      id: uuid.v4(),
      name: entity.name,
      institution: entity.institution,
      accountNumber: entity.accountNumber,
      expenseAccount: entity.expenseAccount,
      currency: entity.currency,
      readerName: entity.readerName,
    };
    this.store.dispatch(accountActionTypes.createAccount({ account }));
  }
}
