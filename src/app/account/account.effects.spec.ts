import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable, throwError, of } from 'rxjs';
import { cold } from 'jasmine-marbles';

import { AccountEffects } from './account.effects';
import {
  loadAccount,
  loadAccounts,
  accountLoaded,
  accountsLoaded,
  createAccount
} from './account.actions';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { SharedModule } from '../shared/shared.module';
import { Actions } from '@ngrx/effects';

const loadAccountsStub = (response: any) => {
  const service = jasmine.createSpyObj('account', ['getAll']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.getAll.and.returnValue(serviceResponse);
  return service;
};

const loadAccountStub = (response: any) => {
  const service = jasmine.createSpyObj('account', ['getOne']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.getOne.and.returnValue(serviceResponse);
  return service;
};

const createAccountStub = (response: any) => {
  const service = jasmine.createSpyObj('account', ['create']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.create.and.returnValue(serviceResponse);
  return service;
};

describe('AccountEffects', () => {
  // tslint:disable-next-line: prefer-const
  let actions$: Observable<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers)],
      providers: [AccountEffects, provideMockActions(() => actions$)],
    });
  });

  it('should be created', () => {
    const effects = TestBed.inject(AccountEffects);
    expect(effects).toBeTruthy();
  });

  it('loadAccounts should return accountsLoaded', () => {
    const source = cold('a', { a: loadAccounts() });
    const service = loadAccountsStub([]);
    const effects = new AccountEffects(service, new Actions(source));

    const expected = cold('a', { a: accountsLoaded({ accounts: [] }) });
    expect(effects.loadAccounts$).toBeObservable(expected);
  });

  it('loadAccount should return accountLoaded', () => {
    const source = cold('a', { a: loadAccount({ key: '42' }) });
    const account = {
      id: '1',
      name: 'Malayan Tapir',
      institution: 'Malayan Tapir',
      accountNumber: 'Malayan Tapir',
      expenseAccount: false,
      currency: 0,
      readerName: 'Malayan Tapir',
    };
    const service = loadAccountStub(account);
    const effects = new AccountEffects(service, new Actions(source));

    const expected = cold('a', { a: accountLoaded({ account }) });
    expect(effects.loadAccount$).toBeObservable(expected);
  });

  it('createAccount should return accountLoaded', () => {
    const account = {
      id: '1',
      name: 'Malayan Tapir',
      institution: 'Malayan Tapir',
      accountNumber: 'Malayan Tapir',
      expenseAccount: false,
      currency: 0,
      readerName: 'Malayan Tapir',
    };
    const source = cold('a', { a: createAccount({ account }) });
    const service = createAccountStub(account);
    const effects = new AccountEffects(service, new Actions(source));

    const expected = cold('a', { a: loadAccounts() });
    expect(effects.createAccount$).toBeObservable(expected);
  });
});
