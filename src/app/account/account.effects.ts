import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AccountService } from './account.service';
import { accountActionTypes } from './account.actions';
import { concatMap, map } from 'rxjs/operators';

@Injectable()
export class AccountEffects {
  constructor(private service: AccountService, private actions$: Actions) {}

  loadAccounts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(accountActionTypes.loadAccounts),
      concatMap(() => this.service.getAll()),
      map((accounts) => accountActionTypes.accountsLoaded({ accounts }))
    )
  );

  loadAccount$ = createEffect(() =>
    this.actions$.pipe(
      ofType(accountActionTypes.loadAccount),
      concatMap((action) => this.service.getOne(action.key)),
      map((account) => accountActionTypes.accountLoaded({ account }))
    )
  );

  createAccount$ = createEffect(() =>
    this.actions$.pipe(
      ofType(accountActionTypes.createAccount),
      concatMap((action) => this.service.create(action.account)),
      map((account) => accountActionTypes.loadAccounts())
    )
  );

  updateAccount$ = createEffect(() =>
    this.actions$.pipe(
      ofType(accountActionTypes.updateAccount),
      concatMap((action) => this.service.update(action.update)),
      map((account) => accountActionTypes.loadAccounts())
    )
  );

  deleteAccounts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(accountActionTypes.deleteAccounts),
      concatMap((action) => this.service.delete(action.keys)),
      map((account) => accountActionTypes.loadAccounts())
    )
  );
}
