import { Currency } from '../currency/currency';
// mastic-pin-import-statements

export interface Account {
  id?: string;
  name: string;
  institution: string;
  accountNumber: string;
  expenseAccount: boolean;
  currency: Currency;
  readerName: string;
}
