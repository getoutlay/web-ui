import { NgModule } from '@angular/core';
import { AccountService } from './account.service';
import { StoreModule } from '@ngrx/store';
import { accountsFeatureKey, accountReducer } from './account.reducer';
import { HttpClientModule } from '@angular/common/http';
import { AccountComponent } from './account.component';
import { AccountFormComponent } from './account-form.component';
import { AccountUpdateComponent } from './account-update.component';
import { SharedModule } from '../shared/shared.module';
import { AccountDeleteDialogComponent } from './account-delete-dialog.component';


@NgModule({
  declarations: [
    AccountComponent,
    AccountFormComponent,
    AccountUpdateComponent,
    AccountDeleteDialogComponent
  ],
  imports: [
    SharedModule,
    HttpClientModule,
    StoreModule.forFeature(accountsFeatureKey, accountReducer),
  ],
  providers: [
    AccountService,
    HttpClientModule,
  ],
})
export class AccountModule { }
