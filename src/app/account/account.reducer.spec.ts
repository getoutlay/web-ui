import { accountReducer, initialState } from './account.reducer';
import {
  createAccount,
  updateAccount,
  deleteAccounts,
  accountLoaded,
  accountsLoaded
} from './account.actions';

describe('Account Reducer', () => {
  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;
      const result = accountReducer(initialState, action);
      expect(result).toBe(initialState);
    });
  });

  describe('updateAccount', () => {
    it('should not update missing one', () => {
      const action = updateAccount({ update: { id: '1', changes: {
        name: 'Malayan Tapir Updated',
        institution: 'Malayan Tapir Updated',
        accountNumber: 'Malayan Tapir Updated',
        expenseAccount: true,
        currency: 0,
        readerName: 'Malayan Tapir Updated',
      } } });
      const result = accountReducer(initialState, action);
      expect(result).toEqual({ ids: [], entities: {} });
    });

    it('should update one', () => {
      const account = {
        id: '1',
        name: 'Malayan Tapir',
        institution: 'Malayan Tapir',
        accountNumber: 'Malayan Tapir',
        expenseAccount: false,
        currency: 0,
        readerName: 'Malayan Tapir',
      };
      const action = updateAccount({ update: { id: '1', changes: {
        name: 'Malayan Tapir Updated',
        institution: 'Malayan Tapir Updated',
        accountNumber: 'Malayan Tapir Updated',
        expenseAccount: true,
        currency: 0,
        readerName: 'Malayan Tapir Updated',
      } } });
      const result = accountReducer({ ids: ['1'], entities: { ['1']: account } }, action);
      expect(result.ids).toEqual(['1']);
      expect(result.entities['1']).toEqual({
        id: '1',
        name: 'Malayan Tapir Updated',
        institution: 'Malayan Tapir Updated',
        accountNumber: 'Malayan Tapir Updated',
        expenseAccount: true,
        currency: 0,
        readerName: 'Malayan Tapir Updated',
      });
    });
  });

  describe('deleteAccounts', () => {
    it('should delete one', () => {
      const account = {
        id: '1',
        name: 'Malayan Tapir',
        institution: 'Malayan Tapir',
        accountNumber: 'Malayan Tapir',
        expenseAccount: false,
        currency: 0,
        readerName: 'Malayan Tapir',
      };
      const action = deleteAccounts({ keys: ['1'] });
      const result = accountReducer({ ids: ['1'], entities: { ['1']: account } }, action);
      expect(result.ids).toEqual([]);
    });
  });

  describe('accountLoaded', () => {
    it('should set one', () => {
      const account = {
        id: '1',
        name: 'Malayan Tapir',
        institution: 'Malayan Tapir',
        accountNumber: 'Malayan Tapir',
        expenseAccount: false,
        currency: 0,
        readerName: 'Malayan Tapir',
      };
      const action = accountLoaded({ account });
      const result = accountReducer(initialState, action);
      expect(result.ids).toEqual(['1']);
    });
  });

  describe('accountsLoaded', () => {
    it('should sets all', () => {
      const accounts = [
        {
          id: '1',
          name: 'Malayan Tapir',
          institution: 'Malayan Tapir',
          accountNumber: 'Malayan Tapir',
          expenseAccount: false,
          currency: 0,
          readerName: 'Malayan Tapir',
        },
        {
          id: '2',
          name: 'Malayan Tapir',
          institution: 'Malayan Tapir',
          accountNumber: 'Malayan Tapir',
          expenseAccount: false,
          currency: 0,
          readerName: 'Malayan Tapir',
        }
      ];
      const action = accountsLoaded({ accounts });
      const result = accountReducer(initialState, action);
      expect(result.ids).toEqual(['1', '2']);
    });
  });
});
