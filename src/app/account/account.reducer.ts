import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Account } from './account.model';
import * as AccountActions from './account.actions';

export const accountsFeatureKey = 'accounts';

export interface AccountState extends EntityState<Account> {
  entity?: Account;
}

export const adapter: EntityAdapter<Account> = createEntityAdapter<Account>();

export const initialState: AccountState = adapter.getInitialState({});

export const accountReducer = createReducer(
  initialState,
  on(AccountActions.updateAccount, (state, action) => adapter.updateOne(action.update, state)),
  on(AccountActions.deleteAccounts, (state, action) => adapter.removeMany(action.keys, state)),
  on(AccountActions.accountLoaded, (state, action) => {
    const entity = action.account;
    return { ...adapter.setOne(entity, state), entity };
  }),
  on(AccountActions.accountsLoaded, (state, action) => adapter.setAll(action.accounts, state))
);

export const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors();
