import { accountFeatureSelector, selectAllAccounts } from './account.selectors';
import { State } from '../reducers';

const state: State = {
  categories: {
    ids: [],
    entities: {}
  },
  transactions: {
    ids: [],
    entities: {}
  },
  transactionCategories: {
    ids: [],
    entities: {}
  },
  inputTypes: {
    ids: [],
    entities: {}
  },
  transactionInputs: {
    ids: [],
    entities: {}
  },
  accounts: {
    ids: ['1', '2', '3'],
    entities: {
      1: { id: '1',
           name: 'Malayan Tapir',
           institution: 'Malayan Tapir',
           accountNumber: 'Malayan Tapir',
           expenseAccount: false,
           currency: 0,
           readerName: 'Malayan Tapir',
      },
      2: { id: '2',
           name: 'Malayan Tapir',
           institution: 'Malayan Tapir',
           accountNumber: 'Malayan Tapir',
           expenseAccount: false,
           currency: 0,
           readerName: 'Malayan Tapir',
      },
      3: { id: '3',
           name: 'Malayan Tapir',
           institution: 'Malayan Tapir',
           accountNumber: 'Malayan Tapir',
           expenseAccount: false,
           currency: 0,
           readerName: 'Malayan Tapir',
      },
    },
  },
};

describe('Account Selectors', () => {
  it('accountFeatureSelector', () => {
    expect(accountFeatureSelector.projector(state.accounts)).toEqual(state.accounts);
  });

  it('getAllAccounts', () => {
    expect(selectAllAccounts.projector(state.accounts)).toEqual([
      state.accounts.entities[1],
      state.accounts.entities[2],
      state.accounts.entities[3],
    ]);
  });
});
