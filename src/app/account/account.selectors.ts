import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AccountState, accountsFeatureKey, selectAll, selectEntities } from './account.reducer';

export const accountFeatureSelector = createFeatureSelector<AccountState>(accountsFeatureKey);
export const selectAllAccounts = createSelector(accountFeatureSelector, selectAll);
export const selectCurrentAccount = createSelector(accountFeatureSelector,
  (accountState) => accountState.entity);
