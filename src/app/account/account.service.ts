import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, pipe, forkJoin } from 'rxjs';
import { Account } from './account.model';
import { Update } from '@ngrx/entity';
import { mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AccountService {
  http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  getAll(): Observable<Account[]> {
    return this.http.get<Account[]>(`/api/accounts`);
  }

  getOne(key: string): Observable<Account> {
    return this.http.get<Account>(`/api/accounts/${key}`);
  }

  create(account: Account): Observable<Account> {
    return this.http.post<Account>(`/api/accounts`, account);
  }

  delete(keys: string[]): Observable<any> {
    return forkJoin(keys.map(key => this.http.delete(`/api/accounts/${key}`)));
  }

  update(update: Update<Account>): Observable<any> {
    return this.http.put(`/api/accounts/${update.id}`, update.changes);
  }
}
