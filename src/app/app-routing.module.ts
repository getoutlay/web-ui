import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { PageNotFoundComponent } from './error/page-not-found.component';
import { AccountComponent } from './account/account.component';
import { AccountUpdateComponent } from './account/account-update.component';
import { CategoryComponent } from './category/category.component';
import { CategoryUpdateComponent } from './category/category-update.component';
import { TransactionComponent } from './transaction/transaction.component';
import { TransactionUpdateComponent } from './transaction/transaction-update.component';
import { TransactionCategoryComponent } from './transaction-category/transaction-category.component';
import { TransactionCategoryUpdateComponent } from './transaction-category/transaction-category-update.component';
import { InputTypeComponent } from './input-type/input-type.component';
import { InputTypeUpdateComponent } from './input-type/input-type-update.component';
import { TransactionInputComponent } from './transaction-input/transaction-input.component';
import { TransactionInputUpdateComponent } from './transaction-input/transaction-input-update.component';
// mastic-pin-import-statements

const routes: Routes = [
  { path: 'welcome', component: WelcomeComponent },
  { path: '', redirectTo: '/welcome', pathMatch: 'full' },
  { path: 'account', component: AccountComponent },
  { path: 'account/:id', component: AccountUpdateComponent },
  { path: 'category', component: CategoryComponent },
  { path: 'category/:id', component: CategoryUpdateComponent },
  { path: 'transaction', component: TransactionComponent },
  { path: 'transaction/:id', component: TransactionUpdateComponent },
  { path: 'transaction-category', component: TransactionCategoryComponent },
  { path: 'transaction-category/:id', component: TransactionCategoryUpdateComponent },
  { path: 'input-type', component: InputTypeComponent },
  { path: 'input-type/:id', component: InputTypeUpdateComponent },
  { path: 'transaction-input', component: TransactionInputComponent },
  { path: 'transaction-input/:id', component: TransactionInputUpdateComponent },
  // mastic-pin-routing-mid-route
  { path: '**', component: PageNotFoundComponent },
  // mastic-pin-routing-last-route
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' }),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
