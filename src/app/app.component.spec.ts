import { TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MediaObserver, MediaChange } from '@angular/flex-layout';
import { TestScheduler } from 'rxjs/testing';
import { RunHelpers } from 'rxjs/internal/testing/TestScheduler';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SideNavigationComponent } from './side-navigation/side-navigation.component';
import { SharedModule } from './shared/shared.module';

class MockMediaObserver {
  public observable: Observable<MediaChange[]>;

  asObservable(): Observable<MediaChange[]> {
    return this.observable;
  }
}

const mediaChanges = (...aliases: string[]): Observable<MediaChange[]> => {
  return from(aliases)
    .pipe(map((alias) => new MediaChange(false, null, alias)))
    .pipe(map((mediaChange) => [mediaChange]));
};

describe('AppComponent', () => {
  let scheduler: TestScheduler;
  const mockMediaObserver = new MockMediaObserver();

  beforeEach(waitForAsync(() => {
    scheduler = new TestScheduler((actual, expected) => {
      expect(actual).toEqual(expected);
    });

    TestBed.configureTestingModule({
      imports: [RouterTestingModule, SharedModule],
      declarations: [AppComponent, HeaderComponent, SideNavigationComponent],
      providers: [{ provide: MediaObserver, useValue: mockMediaObserver }],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should should deliver topGap values ', () => {
    mockMediaObserver.observable = mediaChanges('gt-sm', 'lt-sm', 'md', 'gt-lg');
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;

    scheduler.run((helpers: RunHelpers) => {
      const marbles = '(-l-x-m-g-|)';
      const values = { x: 56, s: 64, m: 64, l: 64, g: 64 };
      helpers.expectObservable(app.topGap).toBe(marbles, values);
    });
  });


  it('should should deliver sideNavOpen values ', () => {
    mockMediaObserver.observable = mediaChanges('gt-md', 'lt-sm', 'md');
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;

    scheduler.run((helpers: RunHelpers) => {
      const marbles = '(-l-x-m-|)';
      const values = { x: false, s: false, m: false, l: true, g: true };
      helpers.expectObservable(app.sideNavOpen).toBe(marbles, values);
    });
  });

  it('should should deliver sideNavMode values ', () => {
    mockMediaObserver.observable = mediaChanges('sm', 'gt-md', 'lt-sm', 'md');
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;

    scheduler.run((helpers: RunHelpers) => {
      const marbles = '(-s-l-x-m-|)';
      const values = { x: 'over', s: 'over', m: 'over', l: 'side', g: 'side' };
      helpers.expectObservable(app.sideNavMode).toBe(marbles, values);
    });
  });
});
