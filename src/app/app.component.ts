import { Component, Input } from '@angular/core';
import { MediaObserver, MediaChange } from '@angular/flex-layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MatDrawerMode } from '@angular/material/sidenav';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
})
export class AppComponent {
  @Input() public topGap: Observable<number>;
  @Input() public sideNavOpen: Observable<boolean>;
  @Input() public sideNavMode: Observable<MatDrawerMode>;

  constructor(mediaObserver: MediaObserver) {
    const mediaEvents = mediaObserver.asObservable();
    if (mediaEvents) {
      this.topGap = mediaEvents.pipe(
        map((changes: MediaChange[]) => {
          if (changes.map((change) => change.mqAlias).includes('lt-sm')) {
            return 56;
          } else {
            return 64;
          }
        })
      );
      this.sideNavOpen = mediaEvents.pipe(
        map((changes: MediaChange[]) => {
          if (changes.map((change) => change.mqAlias).includes('gt-md')) {
            return true;
          } else {
            return false;
          }
        })
      );
      this.sideNavMode = mediaEvents.pipe(
        map((changes: MediaChange[]) => {
          if (changes.map((change) => change.mqAlias).includes('gt-md')) {
            return 'side';
          } else {
            return 'over';
          }
        })
      );
    }
  }
}
