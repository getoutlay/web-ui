import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';

import { WelcomeComponent } from './welcome/welcome.component';
import { HeaderComponent } from './header/header.component';
import { SideNavigationComponent } from './side-navigation/side-navigation.component';
import { PageNotFoundComponent } from './error/page-not-found.component';
import { ElevateDirective } from './elevate.directive';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { reducers, metaReducers } from './reducers';
import { environment } from '../environments/environment';
import { AccountEffects } from './account/account.effects';
import { AccountModule } from './account/account.module';
import { CategoryEffects } from './category/category.effects';
import { CategoryModule } from './category/category.module';
import { TransactionEffects } from './transaction/transaction.effects';
import { TransactionModule } from './transaction/transaction.module';
import { TransactionCategoryEffects } from './transaction-category/transaction-category.effects';
import { TransactionCategoryModule } from './transaction-category/transaction-category.module';
import { InputTypeEffects } from './input-type/input-type.effects';
import { InputTypeModule } from './input-type/input-type.module';
import { TransactionInputEffects } from './transaction-input/transaction-input.effects';
import { TransactionInputModule } from './transaction-input/transaction-input.module';
// mastic-pin-app-import-statements

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    HeaderComponent,
    SideNavigationComponent,
    PageNotFoundComponent,
    ElevateDirective,
    // mastic-pin-app-declarations
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    AccountModule,
    EffectsModule.forRoot([
      AccountEffects,
      CategoryEffects,
      TransactionEffects,
      TransactionCategoryEffects,
      InputTypeEffects,
      TransactionInputEffects,
    ]),
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    CategoryModule,
    TransactionModule,
    TransactionCategoryModule,
    InputTypeModule,
    TransactionInputModule,
    // mastic-pin-app-imports
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
