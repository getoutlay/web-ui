import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Category } from './category.model';

@Component({
  selector: 'app-category-delete-dialog',
  templateUrl: './category-delete-dialog.component.html',
  styleUrls: ['./category-delete-dialog.component.sass'],
})
export class CategoryDeleteDialogComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogInput,
    public dialogRef: MatDialogRef<CategoryDeleteDialogComponent>
  ) {}

  ngOnInit(): void {}
}

export class DialogInput {
  selection: Category[];
}
