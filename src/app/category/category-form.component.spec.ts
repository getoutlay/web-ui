import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CategoryFormComponent } from './category-form.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { RouterModule } from '@angular/router';

const createNewEvent = (eventName: string, bubbles = false, cancelable = false): CustomEvent => {
  const evt = document.createEvent('CustomEvent');
  evt.initCustomEvent(eventName, bubbles, cancelable, null);
  return evt;
};

describe('CategoryFormComponent', () => {
  let component: CategoryFormComponent;
  let fixture: ComponentFixture<CategoryFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers), RouterModule.forRoot([], { relativeLinkResolution: 'legacy' })],
      declarations: [CategoryFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have name', () => {
    expect(component.name).toBeTruthy();
  });

  it('should have colorCode', () => {
    expect(component.colorCode).toBeTruthy();
  });

  it('initially should be invalid', () => {
    expect(component.categoryForm.valid).toBeFalsy();
  });

  it('initially should be valid upon input', () => {
    const event = createNewEvent('input');

    const name = fixture.nativeElement.querySelector('#name');
    name.value = 'Malayan Tapir';
    name.dispatchEvent(event);

    const colorCode = fixture.nativeElement.querySelector('#colorCode');
    colorCode.value = 'Malayan Tapir';
    colorCode.dispatchEvent(event);

    expect(component.categoryForm.valid).toBeTruthy();
  });
});
