import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, AbstractControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { State } from '../reducers';
import { categoryActionTypes } from './category.actions';
import { Observable } from 'rxjs';
import { Category } from './category.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.sass'],
})
export class CategoryFormComponent implements OnInit {
  @Input() category: Observable<Category>;

  categoryForm = new FormGroup({
    id: new FormControl(''),
    name: new FormControl(''),
    colorCode: new FormControl(''),
  });

  constructor(private store: Store<State>, private router: Router) {}

  ngOnInit(): void {
    if (this.category) {
      this.category.subscribe((category) => {
        if (category && category.id) {
          this.categoryForm.setValue(category);
        }
      });
    }
  }

  get name(): FormControl {
    return this.categoryForm.get('name') as FormControl;
  }

  get colorCode(): FormControl {
    return this.categoryForm.get('colorCode') as FormControl;
  }

  onSubmit(): void {
    if (this.categoryForm.valid) {
      if (this.categoryForm.value.id) {
        this.store.dispatch(
          categoryActionTypes.updateCategory({
            update: {
              id: this.categoryForm.value.id,
              changes: this.categoryForm.value,
            },
          })
        );
        this.router.navigate(['/category']);
      } else {
        const value = this.categoryForm.value;
        const { id, ...category } = value;
        this.store.dispatch(categoryActionTypes.createCategory({ category }));
      }
    }
  }
}
