import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Category } from './category.model';
import { Store, select } from '@ngrx/store';
import { State } from '../reducers';
import { selectCurrentCategory } from './category.selectors';
import { categoryActionTypes } from './category.actions';

@Component({
  selector: 'app-category-update',
  templateUrl: './category-update.component.html',
  styleUrls: ['./category-update.component.sass'],
})
export class CategoryUpdateComponent implements OnInit {
  category$: Observable<Category>;

  constructor(private store: Store<State>, private route: ActivatedRoute) {
    this.route.params.pipe(map((params) => params.id)).subscribe((id) => {
      this.store.dispatch(categoryActionTypes.loadCategory({ key: id }));
    });
    this.category$ = this.store.select(selectCurrentCategory);
  }

  ngOnInit(): void {}
}
