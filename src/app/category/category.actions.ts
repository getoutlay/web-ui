import { createAction, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { Category } from './category.model';

export const loadCategories = createAction('[Category/API] Load Categories');
export const categoriesLoaded = createAction('[Category/API] Categories Loaded Successfully',
  props<{ categories: Category[] }>());

export const loadCategory = createAction('[Category/API] Load Category', props<{ key: string }>());
export const categoryLoaded = createAction('[Category/API] Category Loaded Successfully',
  props<{ category: Category }>());

export const createCategory = createAction('[Category/API] Create Category',
  props<{ category: Category }>());

export const deleteCategories = createAction('[Category/API] Delete Categories',
  props<{ keys: string[] }>());

export const updateCategory = createAction('[Category/API] Update Category',
  props<{ update: Update<Category> }>());

export const categoryActionTypes = {
  loadCategories,
  categoriesLoaded,
  loadCategory,
  categoryLoaded,
  createCategory,
  deleteCategories,
  updateCategory,
};
