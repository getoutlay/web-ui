import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Category } from './category.model';
import { selectAllCategories } from './category.selectors';
import { State } from '../reducers';
import { categoryActionTypes } from './category.actions';
import * as uuid from 'uuid';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { CategoryDeleteDialogComponent } from './category-delete-dialog.component';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.sass'],
})
export class CategoryComponent implements OnInit {
  public dataSource: Observable<Category[]>;
  public displayedColumns: string[] = [
    'id',
    'delete',
    'name',
    'colorCode',
  ];
  public selection = new SelectionModel<Category>(true, []);

  constructor(private store: Store<State>, private dialog: MatDialog) {}

  ngOnInit(): void {
    this.store.dispatch(categoryActionTypes.loadCategories());
    this.dataSource = this.store.select(selectAllCategories);
  }

  onDeleteClick(event: Event): void {
    // prevent parent event happening (navigation)
    event.preventDefault();
    event.stopImmediatePropagation();

    const dialogRef = this.dialog.open(CategoryDeleteDialogComponent, {
      data: {
        selection: this.selection.selected,
      },
    });
    dialogRef.afterClosed().subscribe((result: Category[]) => {
      const keys = result.map((entity) => entity.id);
      if (keys && keys.length) {
        this.store.dispatch(categoryActionTypes.deleteCategories({ keys }));
        this.selection.clear();
      }
    });
  }

  toggleSelectAll(): void {
    if (this.selection.hasValue()) {
      this.selection.clear();
    } else {
      this.dataSource.forEach((rows) => rows.forEach((row) => this.selection.select(row)));
    }
  }

  create(entity: Partial<Category>): void {
    const category: Category = {
      id: uuid.v4(),
      name: entity.name,
      colorCode: entity.colorCode,
    };
    this.store.dispatch(categoryActionTypes.createCategory({ category }));
  }
}
