import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable, throwError, of } from 'rxjs';
import { cold } from 'jasmine-marbles';

import { CategoryEffects } from './category.effects';
import {
  loadCategory,
  loadCategories,
  categoryLoaded,
  categoriesLoaded,
  createCategory
} from './category.actions';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { SharedModule } from '../shared/shared.module';
import { Actions } from '@ngrx/effects';

const loadCategoriesStub = (response: any) => {
  const service = jasmine.createSpyObj('category', ['getAll']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.getAll.and.returnValue(serviceResponse);
  return service;
};

const loadCategoryStub = (response: any) => {
  const service = jasmine.createSpyObj('category', ['getOne']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.getOne.and.returnValue(serviceResponse);
  return service;
};

const createCategoryStub = (response: any) => {
  const service = jasmine.createSpyObj('category', ['create']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.create.and.returnValue(serviceResponse);
  return service;
};

describe('CategoryEffects', () => {
  // tslint:disable-next-line: prefer-const
  let actions$: Observable<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers)],
      providers: [CategoryEffects, provideMockActions(() => actions$)],
    });
  });

  it('should be created', () => {
    const effects = TestBed.inject(CategoryEffects);
    expect(effects).toBeTruthy();
  });

  it('loadCategories should return categoriesLoaded', () => {
    const source = cold('a', { a: loadCategories() });
    const service = loadCategoriesStub([]);
    const effects = new CategoryEffects(service, new Actions(source));

    const expected = cold('a', { a: categoriesLoaded({ categories: [] }) });
    expect(effects.loadCategories$).toBeObservable(expected);
  });

  it('loadCategory should return categoryLoaded', () => {
    const source = cold('a', { a: loadCategory({ key: '42' }) });
    const category = {
      id: '1',
      name: 'Malayan Tapir',
      colorCode: 'Malayan Tapir',
    };
    const service = loadCategoryStub(category);
    const effects = new CategoryEffects(service, new Actions(source));

    const expected = cold('a', { a: categoryLoaded({ category }) });
    expect(effects.loadCategory$).toBeObservable(expected);
  });

  it('createCategory should return categoryLoaded', () => {
    const category = {
      id: '1',
      name: 'Malayan Tapir',
      colorCode: 'Malayan Tapir',
    };
    const source = cold('a', { a: createCategory({ category }) });
    const service = createCategoryStub(category);
    const effects = new CategoryEffects(service, new Actions(source));

    const expected = cold('a', { a: loadCategories() });
    expect(effects.createCategory$).toBeObservable(expected);
  });
});
