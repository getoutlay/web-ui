import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { CategoryService } from './category.service';
import { categoryActionTypes } from './category.actions';
import { concatMap, map } from 'rxjs/operators';

@Injectable()
export class CategoryEffects {
  constructor(private service: CategoryService, private actions$: Actions) {}

  loadCategories$ = createEffect(() =>
    this.actions$.pipe(
      ofType(categoryActionTypes.loadCategories),
      concatMap(() => this.service.getAll()),
      map((categories) => categoryActionTypes.categoriesLoaded({ categories }))
    )
  );

  loadCategory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(categoryActionTypes.loadCategory),
      concatMap((action) => this.service.getOne(action.key)),
      map((category) => categoryActionTypes.categoryLoaded({ category }))
    )
  );

  createCategory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(categoryActionTypes.createCategory),
      concatMap((action) => this.service.create(action.category)),
      map((category) => categoryActionTypes.loadCategories())
    )
  );

  updateCategory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(categoryActionTypes.updateCategory),
      concatMap((action) => this.service.update(action.update)),
      map((category) => categoryActionTypes.loadCategories())
    )
  );

  deleteCategories$ = createEffect(() =>
    this.actions$.pipe(
      ofType(categoryActionTypes.deleteCategories),
      concatMap((action) => this.service.delete(action.keys)),
      map((category) => categoryActionTypes.loadCategories())
    )
  );
}
