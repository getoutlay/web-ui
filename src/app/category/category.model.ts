// mastic-pin-import-statements

export interface Category {
  id?: string;
  name: string;
  colorCode: string;
}
