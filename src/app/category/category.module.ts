import { NgModule } from '@angular/core';
import { CategoryService } from './category.service';
import { StoreModule } from '@ngrx/store';
import { categoriesFeatureKey, categoryReducer } from './category.reducer';
import { HttpClientModule } from '@angular/common/http';
import { CategoryComponent } from './category.component';
import { CategoryFormComponent } from './category-form.component';
import { CategoryUpdateComponent } from './category-update.component';
import { SharedModule } from '../shared/shared.module';
import { CategoryDeleteDialogComponent } from './category-delete-dialog.component';


@NgModule({
  declarations: [
    CategoryComponent,
    CategoryFormComponent,
    CategoryUpdateComponent,
    CategoryDeleteDialogComponent
  ],
  imports: [
    SharedModule,
    HttpClientModule,
    StoreModule.forFeature(categoriesFeatureKey, categoryReducer),
  ],
  providers: [
    CategoryService,
    HttpClientModule,
  ],
})
export class CategoryModule { }
