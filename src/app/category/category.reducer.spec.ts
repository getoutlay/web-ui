import { categoryReducer, initialState } from './category.reducer';
import {
  createCategory,
  updateCategory,
  deleteCategories,
  categoryLoaded,
  categoriesLoaded
} from './category.actions';

describe('Category Reducer', () => {
  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;
      const result = categoryReducer(initialState, action);
      expect(result).toBe(initialState);
    });
  });

  describe('updateCategory', () => {
    it('should not update missing one', () => {
      const action = updateCategory({ update: { id: '1', changes: {
        name: 'Malayan Tapir Updated',
        colorCode: 'Malayan Tapir Updated',
      } } });
      const result = categoryReducer(initialState, action);
      expect(result).toEqual({ ids: [], entities: {} });
    });

    it('should update one', () => {
      const category = {
        id: '1',
        name: 'Malayan Tapir',
        colorCode: 'Malayan Tapir',
      };
      const action = updateCategory({ update: { id: '1', changes: {
        name: 'Malayan Tapir Updated',
        colorCode: 'Malayan Tapir Updated',
      } } });
      const result = categoryReducer({ ids: ['1'], entities: { ['1']: category } }, action);
      expect(result.ids).toEqual(['1']);
      expect(result.entities['1']).toEqual({
        id: '1',
        name: 'Malayan Tapir Updated',
        colorCode: 'Malayan Tapir Updated',
      });
    });
  });

  describe('deleteCategories', () => {
    it('should delete one', () => {
      const category = {
        id: '1',
        name: 'Malayan Tapir',
        colorCode: 'Malayan Tapir',
      };
      const action = deleteCategories({ keys: ['1'] });
      const result = categoryReducer({ ids: ['1'], entities: { ['1']: category } }, action);
      expect(result.ids).toEqual([]);
    });
  });

  describe('categoryLoaded', () => {
    it('should set one', () => {
      const category = {
        id: '1',
        name: 'Malayan Tapir',
        colorCode: 'Malayan Tapir',
      };
      const action = categoryLoaded({ category });
      const result = categoryReducer(initialState, action);
      expect(result.ids).toEqual(['1']);
    });
  });

  describe('categoriesLoaded', () => {
    it('should sets all', () => {
      const categories = [
        {
          id: '1',
          name: 'Malayan Tapir',
          colorCode: 'Malayan Tapir',
        },
        {
          id: '2',
          name: 'Malayan Tapir',
          colorCode: 'Malayan Tapir',
        }
      ];
      const action = categoriesLoaded({ categories });
      const result = categoryReducer(initialState, action);
      expect(result.ids).toEqual(['1', '2']);
    });
  });
});
