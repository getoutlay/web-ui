import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Category } from './category.model';
import * as CategoryActions from './category.actions';

export const categoriesFeatureKey = 'categories';

export interface CategoryState extends EntityState<Category> {
  entity?: Category;
}

export const adapter: EntityAdapter<Category> = createEntityAdapter<Category>();

export const initialState: CategoryState = adapter.getInitialState({});

export const categoryReducer = createReducer(
  initialState,
  on(CategoryActions.updateCategory, (state, action) => adapter.updateOne(action.update, state)),
  on(CategoryActions.deleteCategories, (state, action) => adapter.removeMany(action.keys, state)),
  on(CategoryActions.categoryLoaded, (state, action) => {
    const entity = action.category;
    return { ...adapter.setOne(entity, state), entity };
  }),
  on(CategoryActions.categoriesLoaded, (state, action) => adapter.setAll(action.categories, state))
);

export const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors();
