import { categoryFeatureSelector, selectAllCategories } from './category.selectors';
import { State } from '../reducers';

const state: State = {
  accounts: {
    ids: [],
    entities: {}
  },
  transactions: {
    ids: [],
    entities: {}
  },
  transactionCategories: {
    ids: [],
    entities: {}
  },
  inputTypes: {
    ids: [],
    entities: {}
  },
  transactionInputs: {
    ids: [],
    entities: {}
  },
  categories: {
    ids: ['1', '2', '3'],
    entities: {
      1: { id: '1',
           name: 'Malayan Tapir',
           colorCode: 'Malayan Tapir',
      },
      2: { id: '2',
           name: 'Malayan Tapir',
           colorCode: 'Malayan Tapir',
      },
      3: { id: '3',
           name: 'Malayan Tapir',
           colorCode: 'Malayan Tapir',
      },
    },
  },
};

describe('Category Selectors', () => {
  it('categoryFeatureSelector', () => {
    expect(categoryFeatureSelector.projector(state.categories)).toEqual(state.categories);
  });

  it('getAllCategories', () => {
    expect(selectAllCategories.projector(state.categories)).toEqual([
      state.categories.entities[1],
      state.categories.entities[2],
      state.categories.entities[3],
    ]);
  });
});
