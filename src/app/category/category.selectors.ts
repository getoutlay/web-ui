import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CategoryState, categoriesFeatureKey, selectAll, selectEntities } from './category.reducer';

export const categoryFeatureSelector = createFeatureSelector<CategoryState>(categoriesFeatureKey);
export const selectAllCategories = createSelector(categoryFeatureSelector, selectAll);
export const selectCurrentCategory = createSelector(categoryFeatureSelector,
  (categoryState) => categoryState.entity);
