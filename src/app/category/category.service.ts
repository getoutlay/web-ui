import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, pipe, forkJoin } from 'rxjs';
import { Category } from './category.model';
import { Update } from '@ngrx/entity';
import { mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  getAll(): Observable<Category[]> {
    return this.http.get<Category[]>(`/api/categories`);
  }

  getOne(key: string): Observable<Category> {
    return this.http.get<Category>(`/api/categories/${key}`);
  }

  create(category: Category): Observable<Category> {
    return this.http.post<Category>(`/api/categories`, category);
  }

  delete(keys: string[]): Observable<any> {
    return forkJoin(keys.map(key => this.http.delete(`/api/categories/${key}`)));
  }

  update(update: Update<Category>): Observable<any> {
    return this.http.put(`/api/categories/${update.id}`, update.changes);
  }
}
