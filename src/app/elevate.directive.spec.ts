import { ElevateDirective } from './elevate.directive';
import { ElementRef, Component, DebugElement } from '@angular/core';
import { TestBed, ComponentFixture, waitForAsync } from '@angular/core/testing';
import { SharedModule } from './shared/shared.module';
import { By } from '@angular/platform-browser';

class MockElementRef extends ElementRef {}
@Component({
  template: '<button appElevate class="mat-elevation-z4">Test</button>',
})
export class TestButtonComponent {}

describe('ElevateDirective', () => {
  let directive: ElevateDirective;
  let component: TestButtonComponent;
  let fixture: ComponentFixture<TestButtonComponent>;
  let inputEl: DebugElement;
  let spy: any;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [TestButtonComponent, ElevateDirective],
      providers: [{ provide: ElementRef, useClass: MockElementRef }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestButtonComponent);
    component = fixture.componentInstance;
    inputEl = fixture.debugElement.query(By.css('button'));
    directive = fixture.debugElement
      .query(By.directive(ElevateDirective))
      .injector.get(ElevateDirective) as ElevateDirective;
  });

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  it('mouseenter should elevate', () => {
    spy = spyOn(directive, 'setElevation');
    inputEl.triggerEventHandler('mouseenter', null);
    fixture.detectChanges();
    expect(directive.setElevation).toHaveBeenCalledWith(8);
  });

  it('mouseleave event should de-elevate', () => {
    spy = spyOn(directive, 'setElevation');
    inputEl.triggerEventHandler('mouseleave', null);
    fixture.detectChanges();
    expect(directive.setElevation).toHaveBeenCalledWith(2);
  });

  it('mouseenter then mouseleave event should de-elevate', () => {
    spy = spyOn(directive, 'setElevation');
    directive.ngOnChanges(null);
    inputEl.triggerEventHandler('mouseenter', null);
    inputEl.triggerEventHandler('mouseleave', null);
    fixture.detectChanges();
    expect(directive.setElevation).toHaveBeenCalledTimes(3);
  });

});
