export enum InputStatus {
  INITIALIZED,
  RUNNING,
  SUCCESS,
  FAILED,
}
