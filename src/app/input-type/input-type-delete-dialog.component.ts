import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { InputType } from './input-type.model';

@Component({
  selector: 'app-input-type-delete-dialog',
  templateUrl: './input-type-delete-dialog.component.html',
  styleUrls: ['./input-type-delete-dialog.component.sass'],
})
export class InputTypeDeleteDialogComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogInput,
    public dialogRef: MatDialogRef<InputTypeDeleteDialogComponent>
  ) {}

  ngOnInit(): void {}
}

export class DialogInput {
  selection: InputType[];
}
