import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InputTypeFormComponent } from './input-type-form.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { RouterModule } from '@angular/router';

const createNewEvent = (eventName: string, bubbles = false, cancelable = false): CustomEvent => {
  const evt = document.createEvent('CustomEvent');
  evt.initCustomEvent(eventName, bubbles, cancelable, null);
  return evt;
};

describe('InputTypeFormComponent', () => {
  let component: InputTypeFormComponent;
  let fixture: ComponentFixture<InputTypeFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers), RouterModule.forRoot([], { relativeLinkResolution: 'legacy' })],
      declarations: [InputTypeFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputTypeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have name', () => {
    expect(component.name).toBeTruthy();
  });

  it('should have importer', () => {
    expect(component.importer).toBeTruthy();
  });

  it('initially should be invalid', () => {
    expect(component.inputTypeForm.valid).toBeFalsy();
  });

  it('initially should be valid upon input', () => {
    const event = createNewEvent('input');

    const name = fixture.nativeElement.querySelector('#name');
    name.value = 'Malayan Tapir';
    name.dispatchEvent(event);

    const importer = fixture.nativeElement.querySelector('#importer');
    importer.value = 'Malayan Tapir';
    importer.dispatchEvent(event);

    expect(component.inputTypeForm.valid).toBeTruthy();
  });
});
