import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, AbstractControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { State } from '../reducers';
import { inputTypeActionTypes } from './input-type.actions';
import { Observable } from 'rxjs';
import { InputType } from './input-type.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-input-type-form',
  templateUrl: './input-type-form.component.html',
  styleUrls: ['./input-type-form.component.sass'],
})
export class InputTypeFormComponent implements OnInit {
  @Input() inputType: Observable<InputType>;

  inputTypeForm = new FormGroup({
    id: new FormControl(''),
    name: new FormControl(''),
    importer: new FormControl(''),
  });

  constructor(private store: Store<State>, private router: Router) {}

  ngOnInit(): void {
    if (this.inputType) {
      this.inputType.subscribe((inputType) => {
        if (inputType && inputType.id) {
          this.inputTypeForm.setValue(inputType);
        }
      });
    }
  }

  get name(): FormControl {
    return this.inputTypeForm.get('name') as FormControl;
  }

  get importer(): FormControl {
    return this.inputTypeForm.get('importer') as FormControl;
  }

  onSubmit(): void {
    if (this.inputTypeForm.valid) {
      if (this.inputTypeForm.value.id) {
        this.store.dispatch(
          inputTypeActionTypes.updateInputType({
            update: {
              id: this.inputTypeForm.value.id,
              changes: this.inputTypeForm.value,
            },
          })
        );
        this.router.navigate(['/input-type']);
      } else {
        const value = this.inputTypeForm.value;
        const { id, ...inputType } = value;
        this.store.dispatch(inputTypeActionTypes.createInputType({ inputType }));
      }
    }
  }
}
