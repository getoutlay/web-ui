import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InputTypeUpdateComponent } from './input-type-update.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { RouterModule } from '@angular/router';
import { InputTypeFormComponent } from './input-type-form.component';

describe('InputTypeUpdateComponent', () => {
  let component: InputTypeUpdateComponent;
  let fixture: ComponentFixture<InputTypeUpdateComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers), RouterModule.forRoot([], { relativeLinkResolution: 'legacy' })],
      declarations: [InputTypeUpdateComponent, InputTypeFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputTypeUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
