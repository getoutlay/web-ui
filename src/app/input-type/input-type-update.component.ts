import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { InputType } from './input-type.model';
import { Store, select } from '@ngrx/store';
import { State } from '../reducers';
import { selectCurrentInputType } from './input-type.selectors';
import { inputTypeActionTypes } from './input-type.actions';

@Component({
  selector: 'app-input-type-update',
  templateUrl: './input-type-update.component.html',
  styleUrls: ['./input-type-update.component.sass'],
})
export class InputTypeUpdateComponent implements OnInit {
  inputType$: Observable<InputType>;

  constructor(private store: Store<State>, private route: ActivatedRoute) {
    this.route.params.pipe(map((params) => params.id)).subscribe((id) => {
      this.store.dispatch(inputTypeActionTypes.loadInputType({ key: id }));
    });
    this.inputType$ = this.store.select(selectCurrentInputType);
  }

  ngOnInit(): void {}
}
