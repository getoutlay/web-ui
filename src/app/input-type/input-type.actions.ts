import { createAction, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { InputType } from './input-type.model';

export const loadInputTypes = createAction('[InputType/API] Load Input Types');
export const inputTypesLoaded = createAction('[InputType/API] Input Types Loaded Successfully',
  props<{ inputTypes: InputType[] }>());

export const loadInputType = createAction('[InputType/API] Load InputType', props<{ key: string }>());
export const inputTypeLoaded = createAction('[InputType/API] InputType Loaded Successfully',
  props<{ inputType: InputType }>());

export const createInputType = createAction('[InputType/API] Create InputType',
  props<{ inputType: InputType }>());

export const deleteInputTypes = createAction('[InputType/API] Delete Input Types',
  props<{ keys: string[] }>());

export const updateInputType = createAction('[InputType/API] Update InputType',
  props<{ update: Update<InputType> }>());

export const inputTypeActionTypes = {
  loadInputTypes,
  inputTypesLoaded,
  loadInputType,
  inputTypeLoaded,
  createInputType,
  deleteInputTypes,
  updateInputType,
};
