import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { InputType } from './input-type.model';
import { selectAllInputTypes } from './input-type.selectors';
import { State } from '../reducers';
import { inputTypeActionTypes } from './input-type.actions';
import * as uuid from 'uuid';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { InputTypeDeleteDialogComponent } from './input-type-delete-dialog.component';

@Component({
  selector: 'app-input-type',
  templateUrl: './input-type.component.html',
  styleUrls: ['./input-type.component.sass'],
})
export class InputTypeComponent implements OnInit {
  public dataSource: Observable<InputType[]>;
  public displayedColumns: string[] = [
    'id',
    'delete',
    'name',
    'importer',
  ];
  public selection = new SelectionModel<InputType>(true, []);

  constructor(private store: Store<State>, private dialog: MatDialog) {}

  ngOnInit(): void {
    this.store.dispatch(inputTypeActionTypes.loadInputTypes());
    this.dataSource = this.store.select(selectAllInputTypes);
  }

  onDeleteClick(event: Event): void {
    // prevent parent event happening (navigation)
    event.preventDefault();
    event.stopImmediatePropagation();

    const dialogRef = this.dialog.open(InputTypeDeleteDialogComponent, {
      data: {
        selection: this.selection.selected,
      },
    });
    dialogRef.afterClosed().subscribe((result: InputType[]) => {
      const keys = result.map((entity) => entity.id);
      if (keys && keys.length) {
        this.store.dispatch(inputTypeActionTypes.deleteInputTypes({ keys }));
        this.selection.clear();
      }
    });
  }

  toggleSelectAll(): void {
    if (this.selection.hasValue()) {
      this.selection.clear();
    } else {
      this.dataSource.forEach((rows) => rows.forEach((row) => this.selection.select(row)));
    }
  }

  create(entity: Partial<InputType>): void {
    const inputType: InputType = {
      id: uuid.v4(),
      name: entity.name,
      importer: entity.importer,
    };
    this.store.dispatch(inputTypeActionTypes.createInputType({ inputType }));
  }
}
