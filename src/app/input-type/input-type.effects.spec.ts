import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable, throwError, of } from 'rxjs';
import { cold } from 'jasmine-marbles';

import { InputTypeEffects } from './input-type.effects';
import {
  loadInputType,
  loadInputTypes,
  inputTypeLoaded,
  inputTypesLoaded,
  createInputType
} from './input-type.actions';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { SharedModule } from '../shared/shared.module';
import { Actions } from '@ngrx/effects';

const loadInputTypesStub = (response: any) => {
  const service = jasmine.createSpyObj('inputType', ['getAll']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.getAll.and.returnValue(serviceResponse);
  return service;
};

const loadInputTypeStub = (response: any) => {
  const service = jasmine.createSpyObj('inputType', ['getOne']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.getOne.and.returnValue(serviceResponse);
  return service;
};

const createInputTypeStub = (response: any) => {
  const service = jasmine.createSpyObj('inputType', ['create']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.create.and.returnValue(serviceResponse);
  return service;
};

describe('InputTypeEffects', () => {
  // tslint:disable-next-line: prefer-const
  let actions$: Observable<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers)],
      providers: [InputTypeEffects, provideMockActions(() => actions$)],
    });
  });

  it('should be created', () => {
    const effects = TestBed.inject(InputTypeEffects);
    expect(effects).toBeTruthy();
  });

  it('loadInputTypes should return inputTypesLoaded', () => {
    const source = cold('a', { a: loadInputTypes() });
    const service = loadInputTypesStub([]);
    const effects = new InputTypeEffects(service, new Actions(source));

    const expected = cold('a', { a: inputTypesLoaded({ inputTypes: [] }) });
    expect(effects.loadInputTypes$).toBeObservable(expected);
  });

  it('loadInputType should return inputTypeLoaded', () => {
    const source = cold('a', { a: loadInputType({ key: '42' }) });
    const inputType = {
      id: '1',
      name: 'Malayan Tapir',
      importer: 'Malayan Tapir',
    };
    const service = loadInputTypeStub(inputType);
    const effects = new InputTypeEffects(service, new Actions(source));

    const expected = cold('a', { a: inputTypeLoaded({ inputType }) });
    expect(effects.loadInputType$).toBeObservable(expected);
  });

  it('createInputType should return inputTypeLoaded', () => {
    const inputType = {
      id: '1',
      name: 'Malayan Tapir',
      importer: 'Malayan Tapir',
    };
    const source = cold('a', { a: createInputType({ inputType }) });
    const service = createInputTypeStub(inputType);
    const effects = new InputTypeEffects(service, new Actions(source));

    const expected = cold('a', { a: loadInputTypes() });
    expect(effects.createInputType$).toBeObservable(expected);
  });
});
