import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { InputTypeService } from './input-type.service';
import { inputTypeActionTypes } from './input-type.actions';
import { concatMap, map } from 'rxjs/operators';

@Injectable()
export class InputTypeEffects {
  constructor(private service: InputTypeService, private actions$: Actions) {}

  loadInputTypes$ = createEffect(() =>
    this.actions$.pipe(
      ofType(inputTypeActionTypes.loadInputTypes),
      concatMap(() => this.service.getAll()),
      map((inputTypes) => inputTypeActionTypes.inputTypesLoaded({ inputTypes }))
    )
  );

  loadInputType$ = createEffect(() =>
    this.actions$.pipe(
      ofType(inputTypeActionTypes.loadInputType),
      concatMap((action) => this.service.getOne(action.key)),
      map((inputType) => inputTypeActionTypes.inputTypeLoaded({ inputType }))
    )
  );

  createInputType$ = createEffect(() =>
    this.actions$.pipe(
      ofType(inputTypeActionTypes.createInputType),
      concatMap((action) => this.service.create(action.inputType)),
      map((inputType) => inputTypeActionTypes.loadInputTypes())
    )
  );

  updateInputType$ = createEffect(() =>
    this.actions$.pipe(
      ofType(inputTypeActionTypes.updateInputType),
      concatMap((action) => this.service.update(action.update)),
      map((inputType) => inputTypeActionTypes.loadInputTypes())
    )
  );

  deleteInputTypes$ = createEffect(() =>
    this.actions$.pipe(
      ofType(inputTypeActionTypes.deleteInputTypes),
      concatMap((action) => this.service.delete(action.keys)),
      map((inputType) => inputTypeActionTypes.loadInputTypes())
    )
  );
}
