// mastic-pin-import-statements

export interface InputType {
  id?: string;
  name: string;
  importer: string;
}
