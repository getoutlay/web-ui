import { NgModule } from '@angular/core';
import { InputTypeService } from './input-type.service';
import { StoreModule } from '@ngrx/store';
import { inputTypesFeatureKey, inputTypeReducer } from './input-type.reducer';
import { HttpClientModule } from '@angular/common/http';
import { InputTypeComponent } from './input-type.component';
import { InputTypeFormComponent } from './input-type-form.component';
import { InputTypeUpdateComponent } from './input-type-update.component';
import { SharedModule } from '../shared/shared.module';
import { InputTypeDeleteDialogComponent } from './input-type-delete-dialog.component';


@NgModule({
  declarations: [
    InputTypeComponent,
    InputTypeFormComponent,
    InputTypeUpdateComponent,
    InputTypeDeleteDialogComponent
  ],
  imports: [
    SharedModule,
    HttpClientModule,
    StoreModule.forFeature(inputTypesFeatureKey, inputTypeReducer),
  ],
  providers: [
    InputTypeService,
    HttpClientModule,
  ],
})
export class InputTypeModule { }
