import { inputTypeReducer, initialState } from './input-type.reducer';
import {
  createInputType,
  updateInputType,
  deleteInputTypes,
  inputTypeLoaded,
  inputTypesLoaded
} from './input-type.actions';

describe('InputType Reducer', () => {
  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;
      const result = inputTypeReducer(initialState, action);
      expect(result).toBe(initialState);
    });
  });

  describe('updateInputType', () => {
    it('should not update missing one', () => {
      const action = updateInputType({ update: { id: '1', changes: {
        name: 'Malayan Tapir Updated',
        importer: 'Malayan Tapir Updated',
      } } });
      const result = inputTypeReducer(initialState, action);
      expect(result).toEqual({ ids: [], entities: {} });
    });

    it('should update one', () => {
      const inputType = {
        id: '1',
        name: 'Malayan Tapir',
        importer: 'Malayan Tapir',
      };
      const action = updateInputType({ update: { id: '1', changes: {
        name: 'Malayan Tapir Updated',
        importer: 'Malayan Tapir Updated',
      } } });
      const result = inputTypeReducer({ ids: ['1'], entities: { ['1']: inputType } }, action);
      expect(result.ids).toEqual(['1']);
      expect(result.entities['1']).toEqual({
        id: '1',
        name: 'Malayan Tapir Updated',
        importer: 'Malayan Tapir Updated',
      });
    });
  });

  describe('deleteInputTypes', () => {
    it('should delete one', () => {
      const inputType = {
        id: '1',
        name: 'Malayan Tapir',
        importer: 'Malayan Tapir',
      };
      const action = deleteInputTypes({ keys: ['1'] });
      const result = inputTypeReducer({ ids: ['1'], entities: { ['1']: inputType } }, action);
      expect(result.ids).toEqual([]);
    });
  });

  describe('inputTypeLoaded', () => {
    it('should set one', () => {
      const inputType = {
        id: '1',
        name: 'Malayan Tapir',
        importer: 'Malayan Tapir',
      };
      const action = inputTypeLoaded({ inputType });
      const result = inputTypeReducer(initialState, action);
      expect(result.ids).toEqual(['1']);
    });
  });

  describe('inputTypesLoaded', () => {
    it('should sets all', () => {
      const inputTypes = [
        {
          id: '1',
          name: 'Malayan Tapir',
          importer: 'Malayan Tapir',
        },
        {
          id: '2',
          name: 'Malayan Tapir',
          importer: 'Malayan Tapir',
        }
      ];
      const action = inputTypesLoaded({ inputTypes });
      const result = inputTypeReducer(initialState, action);
      expect(result.ids).toEqual(['1', '2']);
    });
  });
});
