import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { InputType } from './input-type.model';
import * as InputTypeActions from './input-type.actions';

export const inputTypesFeatureKey = 'inputTypes';

export interface InputTypeState extends EntityState<InputType> {
  entity?: InputType;
}

export const adapter: EntityAdapter<InputType> = createEntityAdapter<InputType>();

export const initialState: InputTypeState = adapter.getInitialState({});

export const inputTypeReducer = createReducer(
  initialState,
  on(InputTypeActions.updateInputType, (state, action) => adapter.updateOne(action.update, state)),
  on(InputTypeActions.deleteInputTypes, (state, action) => adapter.removeMany(action.keys, state)),
  on(InputTypeActions.inputTypeLoaded, (state, action) => {
    const entity = action.inputType;
    return { ...adapter.setOne(entity, state), entity };
  }),
  on(InputTypeActions.inputTypesLoaded, (state, action) => adapter.setAll(action.inputTypes, state))
);

export const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors();
