import { inputTypeFeatureSelector, selectAllInputTypes } from './input-type.selectors';
import { State } from '../reducers';

const state: State = {
  accounts: {
    ids: [],
    entities: {}
  },
  categories: {
    ids: [],
    entities: {}
  },
  transactions: {
    ids: [],
    entities: {}
  },
  transactionCategories: {
    ids: [],
    entities: {}
  },
  transactionInputs: {
    ids: [],
    entities: {}
  },
  inputTypes: {
    ids: ['1', '2', '3'],
    entities: {
      1: { id: '1',
           name: 'Malayan Tapir',
           importer: 'Malayan Tapir',
      },
      2: { id: '2',
           name: 'Malayan Tapir',
           importer: 'Malayan Tapir',
      },
      3: { id: '3',
           name: 'Malayan Tapir',
           importer: 'Malayan Tapir',
      },
    },
  },
};

describe('InputType Selectors', () => {
  it('inputTypeFeatureSelector', () => {
    expect(inputTypeFeatureSelector.projector(state.inputTypes)).toEqual(state.inputTypes);
  });

  it('getAllInputTypes', () => {
    expect(selectAllInputTypes.projector(state.inputTypes)).toEqual([
      state.inputTypes.entities[1],
      state.inputTypes.entities[2],
      state.inputTypes.entities[3],
    ]);
  });
});
