import { createFeatureSelector, createSelector } from '@ngrx/store';
import { InputTypeState, inputTypesFeatureKey, selectAll, selectEntities } from './input-type.reducer';

export const inputTypeFeatureSelector = createFeatureSelector<InputTypeState>(inputTypesFeatureKey);
export const selectAllInputTypes = createSelector(inputTypeFeatureSelector, selectAll);
export const selectCurrentInputType = createSelector(inputTypeFeatureSelector,
  (inputTypeState) => inputTypeState.entity);
