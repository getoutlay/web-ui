import { TestBed } from '@angular/core/testing';

import { InputTypeService } from './input-type.service';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { SharedModule } from '../shared/shared.module';

describe('InputTypeService', () => {
  let service: InputTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers)],
    });
    service = TestBed.inject(InputTypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
