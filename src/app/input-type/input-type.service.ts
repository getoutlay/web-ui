import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, pipe, forkJoin } from 'rxjs';
import { InputType } from './input-type.model';
import { Update } from '@ngrx/entity';
import { mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class InputTypeService {
  http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  getAll(): Observable<InputType[]> {
    return this.http.get<InputType[]>(`/api/input-types`);
  }

  getOne(key: string): Observable<InputType> {
    return this.http.get<InputType>(`/api/input-types/${key}`);
  }

  create(inputType: InputType): Observable<InputType> {
    return this.http.post<InputType>(`/api/input-types`, inputType);
  }

  delete(keys: string[]): Observable<any> {
    return forkJoin(keys.map(key => this.http.delete(`/api/input-types/${key}`)));
  }

  update(update: Update<InputType>): Observable<any> {
    return this.http.put(`/api/input-types/${update.id}`, update.changes);
  }
}
