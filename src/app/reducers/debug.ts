import { ActionReducer } from '@ngrx/store';

// console.log all actions
export function debug(reducer: ActionReducer<any>): ActionReducer<any> {
  return (state, action) => {
    console.log(`state: ${state}, action: ${action}`);
    return reducer(state, action);
  };
}
