import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { environment } from '../../environments/environment';
import { debug } from './debug';
import * as fromAccount from '../account/account.reducer';
import * as fromCategory from '../category/category.reducer';
import * as fromTransaction from '../transaction/transaction.reducer';
import * as fromTransactionCategory from '../transaction-category/transaction-category.reducer';
import * as fromInputType from '../input-type/input-type.reducer';
import * as fromTransactionInput from '../transaction-input/transaction-input.reducer';

export interface State {
  [fromAccount.accountsFeatureKey]: fromAccount.AccountState;
  [fromCategory.categoriesFeatureKey]: fromCategory.CategoryState;
  [fromTransaction.transactionsFeatureKey]: fromTransaction.TransactionState;
  [fromTransactionCategory.transactionCategoriesFeatureKey]: fromTransactionCategory.TransactionCategoryState;
  [fromInputType.inputTypesFeatureKey]: fromInputType.InputTypeState;
  [fromTransactionInput.transactionInputsFeatureKey]: fromTransactionInput.TransactionInputState;
}

export const reducers: ActionReducerMap<State> = {
  [fromAccount.accountsFeatureKey]: fromAccount.accountReducer,
  [fromCategory.categoriesFeatureKey]: fromCategory.categoryReducer,
  [fromTransaction.transactionsFeatureKey]: fromTransaction.transactionReducer,
  [fromTransactionCategory.transactionCategoriesFeatureKey]: fromTransactionCategory.transactionCategoryReducer,
  [fromInputType.inputTypesFeatureKey]: fromInputType.inputTypeReducer,
  [fromTransactionInput.transactionInputsFeatureKey]: fromTransactionInput.transactionInputReducer,
};

export const metaReducers: MetaReducer<State>[] = !environment.production ? [debug] : [];
