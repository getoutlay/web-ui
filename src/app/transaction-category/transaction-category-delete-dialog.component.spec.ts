import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TransactionCategoryDeleteDialogComponent } from './transaction-category-delete-dialog.component';
import { SharedModule } from '../shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

describe('TransactionCategoryDeleteDialogComponent', () => {
  let component: TransactionCategoryDeleteDialogComponent;
  let fixture: ComponentFixture<TransactionCategoryDeleteDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TransactionCategoryDeleteDialogComponent],
      imports: [SharedModule, BrowserAnimationsModule],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            selection: [{
              id: '1',
              assignedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
              assignedBy: 'Malayan Tapir',
            }],
          },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionCategoryDeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have data', () => {
    expect(component.data).toBeTruthy();
    expect(component.data.selection).toBeTruthy();
    expect(component.data.selection.length).toEqual(1);
  });

});
