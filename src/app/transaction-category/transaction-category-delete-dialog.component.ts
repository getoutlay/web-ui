import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { TransactionCategory } from './transaction-category.model';

@Component({
  selector: 'app-transaction-category-delete-dialog',
  templateUrl: './transaction-category-delete-dialog.component.html',
  styleUrls: ['./transaction-category-delete-dialog.component.sass'],
})
export class TransactionCategoryDeleteDialogComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogInput,
    public dialogRef: MatDialogRef<TransactionCategoryDeleteDialogComponent>
  ) {}

  ngOnInit(): void {}
}

export class DialogInput {
  selection: TransactionCategory[];
}
