import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TransactionCategoryFormComponent } from './transaction-category-form.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { RouterModule } from '@angular/router';

const createNewEvent = (eventName: string, bubbles = false, cancelable = false): CustomEvent => {
  const evt = document.createEvent('CustomEvent');
  evt.initCustomEvent(eventName, bubbles, cancelable, null);
  return evt;
};

describe('TransactionCategoryFormComponent', () => {
  let component: TransactionCategoryFormComponent;
  let fixture: ComponentFixture<TransactionCategoryFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers), RouterModule.forRoot([], { relativeLinkResolution: 'legacy' })],
      declarations: [TransactionCategoryFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionCategoryFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have assignedOn', () => {
    expect(component.assignedOn).toBeTruthy();
  });

  it('should have assignedBy', () => {
    expect(component.assignedBy).toBeTruthy();
  });

  it('initially should be invalid', () => {
    expect(component.transactionCategoryForm.valid).toBeFalsy();
  });

  it('initially should be valid upon input', () => {
    const event = createNewEvent('input');

    const assignedOn = fixture.nativeElement.querySelector('#assignedOn');
    assignedOn.value = new Date(Date.UTC(2020, 1, 0, 9, 0)).toISOString().split('T')[0];
    assignedOn.dispatchEvent(event);

    const assignedBy = fixture.nativeElement.querySelector('#assignedBy');
    assignedBy.value = 'Malayan Tapir';
    assignedBy.dispatchEvent(event);

    expect(component.transactionCategoryForm.valid).toBeTruthy();
  });
});
