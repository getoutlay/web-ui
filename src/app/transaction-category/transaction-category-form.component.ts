import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, AbstractControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { State } from '../reducers';
import { transactionCategoryActionTypes } from './transaction-category.actions';
import { Observable } from 'rxjs';
import { TransactionCategory } from './transaction-category.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-transaction-category-form',
  templateUrl: './transaction-category-form.component.html',
  styleUrls: ['./transaction-category-form.component.sass'],
})
export class TransactionCategoryFormComponent implements OnInit {
  @Input() transactionCategory: Observable<TransactionCategory>;

  transactionCategoryForm = new FormGroup({
    id: new FormControl(''),
    assignedOn: new FormControl(''),
    assignedBy: new FormControl(''),
  });

  constructor(private store: Store<State>, private router: Router) {}

  ngOnInit(): void {
    if (this.transactionCategory) {
      this.transactionCategory.subscribe((transactionCategory) => {
        if (transactionCategory && transactionCategory.id) {
          this.transactionCategoryForm.setValue(transactionCategory);
        }
      });
    }
  }

  get assignedOn(): FormControl {
    return this.transactionCategoryForm.get('assignedOn') as FormControl;
  }

  get assignedBy(): FormControl {
    return this.transactionCategoryForm.get('assignedBy') as FormControl;
  }

  onSubmit(): void {
    if (this.transactionCategoryForm.valid) {
      if (this.transactionCategoryForm.value.id) {
        this.store.dispatch(
          transactionCategoryActionTypes.updateTransactionCategory({
            update: {
              id: this.transactionCategoryForm.value.id,
              changes: this.transactionCategoryForm.value,
            },
          })
        );
        this.router.navigate(['/transaction-category']);
      } else {
        const value = this.transactionCategoryForm.value;
        const { id, ...transactionCategory } = value;
        this.store.dispatch(transactionCategoryActionTypes.createTransactionCategory({ transactionCategory }));
      }
    }
  }
}
