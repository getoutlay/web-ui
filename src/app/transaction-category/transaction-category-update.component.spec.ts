import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TransactionCategoryUpdateComponent } from './transaction-category-update.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { RouterModule } from '@angular/router';
import { TransactionCategoryFormComponent } from './transaction-category-form.component';

describe('TransactionCategoryUpdateComponent', () => {
  let component: TransactionCategoryUpdateComponent;
  let fixture: ComponentFixture<TransactionCategoryUpdateComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers), RouterModule.forRoot([], { relativeLinkResolution: 'legacy' })],
      declarations: [TransactionCategoryUpdateComponent, TransactionCategoryFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionCategoryUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
