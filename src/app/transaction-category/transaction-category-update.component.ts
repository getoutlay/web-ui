import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { TransactionCategory } from './transaction-category.model';
import { Store, select } from '@ngrx/store';
import { State } from '../reducers';
import { selectCurrentTransactionCategory } from './transaction-category.selectors';
import { transactionCategoryActionTypes } from './transaction-category.actions';

@Component({
  selector: 'app-transaction-category-update',
  templateUrl: './transaction-category-update.component.html',
  styleUrls: ['./transaction-category-update.component.sass'],
})
export class TransactionCategoryUpdateComponent implements OnInit {
  transactionCategory$: Observable<TransactionCategory>;

  constructor(private store: Store<State>, private route: ActivatedRoute) {
    this.route.params.pipe(map((params) => params.id)).subscribe((id) => {
      this.store.dispatch(transactionCategoryActionTypes.loadTransactionCategory({ key: id }));
    });
    this.transactionCategory$ = this.store.select(selectCurrentTransactionCategory);
  }

  ngOnInit(): void {}
}
