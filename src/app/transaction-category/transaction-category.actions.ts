import { createAction, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { TransactionCategory } from './transaction-category.model';

export const loadTransactionCategories = createAction('[TransactionCategory/API] Load Transaction Categories');
export const transactionCategoriesLoaded = createAction('[TransactionCategory/API] Transaction Categories Loaded Successfully',
  props<{ transactionCategories: TransactionCategory[] }>());

export const loadTransactionCategory = createAction('[TransactionCategory/API] Load TransactionCategory', props<{ key: string }>());
export const transactionCategoryLoaded = createAction('[TransactionCategory/API] TransactionCategory Loaded Successfully',
  props<{ transactionCategory: TransactionCategory }>());

export const createTransactionCategory = createAction('[TransactionCategory/API] Create TransactionCategory',
  props<{ transactionCategory: TransactionCategory }>());

export const deleteTransactionCategories = createAction('[TransactionCategory/API] Delete Transaction Categories',
  props<{ keys: string[] }>());

export const updateTransactionCategory = createAction('[TransactionCategory/API] Update TransactionCategory',
  props<{ update: Update<TransactionCategory> }>());

export const transactionCategoryActionTypes = {
  loadTransactionCategories,
  transactionCategoriesLoaded,
  loadTransactionCategory,
  transactionCategoryLoaded,
  createTransactionCategory,
  deleteTransactionCategories,
  updateTransactionCategory,
};
