import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TransactionCategoryComponent } from './transaction-category.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { TransactionCategoryFormComponent } from './transaction-category-form.component';
import { RouterModule } from '@angular/router';

describe('TransactionCategoryComponent', () => {
  let component: TransactionCategoryComponent;
  let fixture: ComponentFixture<TransactionCategoryComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers), RouterModule.forRoot([], { relativeLinkResolution: 'legacy' })],
      declarations: [TransactionCategoryComponent, TransactionCategoryFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
