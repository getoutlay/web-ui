import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { TransactionCategory } from './transaction-category.model';
import { selectAllTransactionCategories } from './transaction-category.selectors';
import { State } from '../reducers';
import { transactionCategoryActionTypes } from './transaction-category.actions';
import * as uuid from 'uuid';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { TransactionCategoryDeleteDialogComponent } from './transaction-category-delete-dialog.component';

@Component({
  selector: 'app-transaction-category',
  templateUrl: './transaction-category.component.html',
  styleUrls: ['./transaction-category.component.sass'],
})
export class TransactionCategoryComponent implements OnInit {
  public dataSource: Observable<TransactionCategory[]>;
  public displayedColumns: string[] = [
    'id',
    'delete',
    'assignedOn',
    'assignedBy',
  ];
  public selection = new SelectionModel<TransactionCategory>(true, []);

  constructor(private store: Store<State>, private dialog: MatDialog) {}

  ngOnInit(): void {
    this.store.dispatch(transactionCategoryActionTypes.loadTransactionCategories());
    this.dataSource = this.store.select(selectAllTransactionCategories);
  }

  onDeleteClick(event: Event): void {
    // prevent parent event happening (navigation)
    event.preventDefault();
    event.stopImmediatePropagation();

    const dialogRef = this.dialog.open(TransactionCategoryDeleteDialogComponent, {
      data: {
        selection: this.selection.selected,
      },
    });
    dialogRef.afterClosed().subscribe((result: TransactionCategory[]) => {
      const keys = result.map((entity) => entity.id);
      if (keys && keys.length) {
        this.store.dispatch(transactionCategoryActionTypes.deleteTransactionCategories({ keys }));
        this.selection.clear();
      }
    });
  }

  toggleSelectAll(): void {
    if (this.selection.hasValue()) {
      this.selection.clear();
    } else {
      this.dataSource.forEach((rows) => rows.forEach((row) => this.selection.select(row)));
    }
  }

  create(entity: Partial<TransactionCategory>): void {
    const transactionCategory: TransactionCategory = {
      id: uuid.v4(),
      assignedOn: entity.assignedOn,
      assignedBy: entity.assignedBy,
    };
    this.store.dispatch(transactionCategoryActionTypes.createTransactionCategory({ transactionCategory }));
  }
}
