import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable, throwError, of } from 'rxjs';
import { cold } from 'jasmine-marbles';

import { TransactionCategoryEffects } from './transaction-category.effects';
import {
  loadTransactionCategory,
  loadTransactionCategories,
  transactionCategoryLoaded,
  transactionCategoriesLoaded,
  createTransactionCategory
} from './transaction-category.actions';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { SharedModule } from '../shared/shared.module';
import { Actions } from '@ngrx/effects';

const loadTransactionCategoriesStub = (response: any) => {
  const service = jasmine.createSpyObj('transactionCategory', ['getAll']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.getAll.and.returnValue(serviceResponse);
  return service;
};

const loadTransactionCategoryStub = (response: any) => {
  const service = jasmine.createSpyObj('transactionCategory', ['getOne']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.getOne.and.returnValue(serviceResponse);
  return service;
};

const createTransactionCategoryStub = (response: any) => {
  const service = jasmine.createSpyObj('transactionCategory', ['create']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.create.and.returnValue(serviceResponse);
  return service;
};

describe('TransactionCategoryEffects', () => {
  // tslint:disable-next-line: prefer-const
  let actions$: Observable<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers)],
      providers: [TransactionCategoryEffects, provideMockActions(() => actions$)],
    });
  });

  it('should be created', () => {
    const effects = TestBed.inject(TransactionCategoryEffects);
    expect(effects).toBeTruthy();
  });

  it('loadTransactionCategories should return transactionCategoriesLoaded', () => {
    const source = cold('a', { a: loadTransactionCategories() });
    const service = loadTransactionCategoriesStub([]);
    const effects = new TransactionCategoryEffects(service, new Actions(source));

    const expected = cold('a', { a: transactionCategoriesLoaded({ transactionCategories: [] }) });
    expect(effects.loadTransactionCategories$).toBeObservable(expected);
  });

  it('loadTransactionCategory should return transactionCategoryLoaded', () => {
    const source = cold('a', { a: loadTransactionCategory({ key: '42' }) });
    const transactionCategory = {
      id: '1',
      assignedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
      assignedBy: 'Malayan Tapir',
    };
    const service = loadTransactionCategoryStub(transactionCategory);
    const effects = new TransactionCategoryEffects(service, new Actions(source));

    const expected = cold('a', { a: transactionCategoryLoaded({ transactionCategory }) });
    expect(effects.loadTransactionCategory$).toBeObservable(expected);
  });

  it('createTransactionCategory should return transactionCategoryLoaded', () => {
    const transactionCategory = {
      id: '1',
      assignedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
      assignedBy: 'Malayan Tapir',
    };
    const source = cold('a', { a: createTransactionCategory({ transactionCategory }) });
    const service = createTransactionCategoryStub(transactionCategory);
    const effects = new TransactionCategoryEffects(service, new Actions(source));

    const expected = cold('a', { a: loadTransactionCategories() });
    expect(effects.createTransactionCategory$).toBeObservable(expected);
  });
});
