import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { TransactionCategoryService } from './transaction-category.service';
import { transactionCategoryActionTypes } from './transaction-category.actions';
import { concatMap, map } from 'rxjs/operators';

@Injectable()
export class TransactionCategoryEffects {
  constructor(private service: TransactionCategoryService, private actions$: Actions) {}

  loadTransactionCategories$ = createEffect(() =>
    this.actions$.pipe(
      ofType(transactionCategoryActionTypes.loadTransactionCategories),
      concatMap(() => this.service.getAll()),
      map((transactionCategories) => transactionCategoryActionTypes.transactionCategoriesLoaded({ transactionCategories }))
    )
  );

  loadTransactionCategory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(transactionCategoryActionTypes.loadTransactionCategory),
      concatMap((action) => this.service.getOne(action.key)),
      map((transactionCategory) => transactionCategoryActionTypes.transactionCategoryLoaded({ transactionCategory }))
    )
  );

  createTransactionCategory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(transactionCategoryActionTypes.createTransactionCategory),
      concatMap((action) => this.service.create(action.transactionCategory)),
      map((transactionCategory) => transactionCategoryActionTypes.loadTransactionCategories())
    )
  );

  updateTransactionCategory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(transactionCategoryActionTypes.updateTransactionCategory),
      concatMap((action) => this.service.update(action.update)),
      map((transactionCategory) => transactionCategoryActionTypes.loadTransactionCategories())
    )
  );

  deleteTransactionCategories$ = createEffect(() =>
    this.actions$.pipe(
      ofType(transactionCategoryActionTypes.deleteTransactionCategories),
      concatMap((action) => this.service.delete(action.keys)),
      map((transactionCategory) => transactionCategoryActionTypes.loadTransactionCategories())
    )
  );
}
