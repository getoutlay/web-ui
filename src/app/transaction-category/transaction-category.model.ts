// mastic-pin-import-statements

export interface TransactionCategory {
  id?: string;
  assignedOn: Date;
  assignedBy: string;
}
