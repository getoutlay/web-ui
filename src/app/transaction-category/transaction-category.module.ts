import { NgModule } from '@angular/core';
import { TransactionCategoryService } from './transaction-category.service';
import { StoreModule } from '@ngrx/store';
import { transactionCategoriesFeatureKey, transactionCategoryReducer } from './transaction-category.reducer';
import { HttpClientModule } from '@angular/common/http';
import { TransactionCategoryComponent } from './transaction-category.component';
import { TransactionCategoryFormComponent } from './transaction-category-form.component';
import { TransactionCategoryUpdateComponent } from './transaction-category-update.component';
import { SharedModule } from '../shared/shared.module';
import { TransactionCategoryDeleteDialogComponent } from './transaction-category-delete-dialog.component';


@NgModule({
  declarations: [
    TransactionCategoryComponent,
    TransactionCategoryFormComponent,
    TransactionCategoryUpdateComponent,
    TransactionCategoryDeleteDialogComponent
  ],
  imports: [
    SharedModule,
    HttpClientModule,
    StoreModule.forFeature(transactionCategoriesFeatureKey, transactionCategoryReducer),
  ],
  providers: [
    TransactionCategoryService,
    HttpClientModule,
  ],
})
export class TransactionCategoryModule { }
