import { transactionCategoryReducer, initialState } from './transaction-category.reducer';
import {
  createTransactionCategory,
  updateTransactionCategory,
  deleteTransactionCategories,
  transactionCategoryLoaded,
  transactionCategoriesLoaded
} from './transaction-category.actions';

describe('TransactionCategory Reducer', () => {
  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;
      const result = transactionCategoryReducer(initialState, action);
      expect(result).toBe(initialState);
    });
  });

  describe('updateTransactionCategory', () => {
    it('should not update missing one', () => {
      const action = updateTransactionCategory({ update: { id: '1', changes: {
        assignedOn: new Date(Date.UTC(2020, 1, 1, 9, 0)),
        assignedBy: 'Malayan Tapir Updated',
      } } });
      const result = transactionCategoryReducer(initialState, action);
      expect(result).toEqual({ ids: [], entities: {} });
    });

    it('should update one', () => {
      const transactionCategory = {
        id: '1',
        assignedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
        assignedBy: 'Malayan Tapir',
      };
      const action = updateTransactionCategory({ update: { id: '1', changes: {
        assignedOn: new Date(Date.UTC(2020, 1, 1, 9, 0)),
        assignedBy: 'Malayan Tapir Updated',
      } } });
      const result = transactionCategoryReducer({ ids: ['1'], entities: { ['1']: transactionCategory } }, action);
      expect(result.ids).toEqual(['1']);
      expect(result.entities['1']).toEqual({
        id: '1',
        assignedOn: new Date(Date.UTC(2020, 1, 1, 9, 0)),
        assignedBy: 'Malayan Tapir Updated',
      });
    });
  });

  describe('deleteTransactionCategories', () => {
    it('should delete one', () => {
      const transactionCategory = {
        id: '1',
        assignedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
        assignedBy: 'Malayan Tapir',
      };
      const action = deleteTransactionCategories({ keys: ['1'] });
      const result = transactionCategoryReducer({ ids: ['1'], entities: { ['1']: transactionCategory } }, action);
      expect(result.ids).toEqual([]);
    });
  });

  describe('transactionCategoryLoaded', () => {
    it('should set one', () => {
      const transactionCategory = {
        id: '1',
        assignedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
        assignedBy: 'Malayan Tapir',
      };
      const action = transactionCategoryLoaded({ transactionCategory });
      const result = transactionCategoryReducer(initialState, action);
      expect(result.ids).toEqual(['1']);
    });
  });

  describe('transactionCategoriesLoaded', () => {
    it('should sets all', () => {
      const transactionCategories = [
        {
          id: '1',
          assignedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
          assignedBy: 'Malayan Tapir',
        },
        {
          id: '2',
          assignedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
          assignedBy: 'Malayan Tapir',
        }
      ];
      const action = transactionCategoriesLoaded({ transactionCategories });
      const result = transactionCategoryReducer(initialState, action);
      expect(result.ids).toEqual(['1', '2']);
    });
  });
});
