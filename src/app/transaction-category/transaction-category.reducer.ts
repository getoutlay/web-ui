import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { TransactionCategory } from './transaction-category.model';
import * as TransactionCategoryActions from './transaction-category.actions';

export const transactionCategoriesFeatureKey = 'transactionCategories';

export interface TransactionCategoryState extends EntityState<TransactionCategory> {
  entity?: TransactionCategory;
}

export const adapter: EntityAdapter<TransactionCategory> = createEntityAdapter<TransactionCategory>();

export const initialState: TransactionCategoryState = adapter.getInitialState({});

export const transactionCategoryReducer = createReducer(
  initialState,
  on(TransactionCategoryActions.updateTransactionCategory, (state, action) => adapter.updateOne(action.update, state)),
  on(TransactionCategoryActions.deleteTransactionCategories, (state, action) => adapter.removeMany(action.keys, state)),
  on(TransactionCategoryActions.transactionCategoryLoaded, (state, action) => {
    const entity = action.transactionCategory;
    return { ...adapter.setOne(entity, state), entity };
  }),
  on(TransactionCategoryActions.transactionCategoriesLoaded, (state, action) => adapter.setAll(action.transactionCategories, state))
);

export const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors();
