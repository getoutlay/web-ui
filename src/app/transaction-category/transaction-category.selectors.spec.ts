import { transactionCategoryFeatureSelector, selectAllTransactionCategories } from './transaction-category.selectors';
import { State } from '../reducers';

const state: State = {
  accounts: {
    ids: [],
    entities: {}
  },
  categories: {
    ids: [],
    entities: {}
  },
  transactions: {
    ids: [],
    entities: {}
  },
  inputTypes: {
    ids: [],
    entities: {}
  },
  transactionInputs: {
    ids: [],
    entities: {}
  },
  transactionCategories: {
    ids: ['1', '2', '3'],
    entities: {
      1: { id: '1',
           assignedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
           assignedBy: 'Malayan Tapir',
      },
      2: { id: '2',
           assignedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
           assignedBy: 'Malayan Tapir',
      },
      3: { id: '3',
           assignedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
           assignedBy: 'Malayan Tapir',
      },
    },
  },
};

describe('TransactionCategory Selectors', () => {
  it('transactionCategoryFeatureSelector', () => {
    expect(transactionCategoryFeatureSelector.projector(state.transactionCategories)).toEqual(state.transactionCategories);
  });

  it('getAllTransactionCategories', () => {
    expect(selectAllTransactionCategories.projector(state.transactionCategories)).toEqual([
      state.transactionCategories.entities[1],
      state.transactionCategories.entities[2],
      state.transactionCategories.entities[3],
    ]);
  });
});
