import { createFeatureSelector, createSelector } from '@ngrx/store';
import { TransactionCategoryState, transactionCategoriesFeatureKey, selectAll, selectEntities } from './transaction-category.reducer';

export const transactionCategoryFeatureSelector = createFeatureSelector<TransactionCategoryState>(transactionCategoriesFeatureKey);
export const selectAllTransactionCategories = createSelector(transactionCategoryFeatureSelector, selectAll);
export const selectCurrentTransactionCategory = createSelector(transactionCategoryFeatureSelector,
  (transactionCategoryState) => transactionCategoryState.entity);
