import { TestBed } from '@angular/core/testing';

import { TransactionCategoryService } from './transaction-category.service';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { SharedModule } from '../shared/shared.module';

describe('TransactionCategoryService', () => {
  let service: TransactionCategoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers)],
    });
    service = TestBed.inject(TransactionCategoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
