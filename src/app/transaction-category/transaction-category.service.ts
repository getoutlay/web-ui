import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, pipe, forkJoin } from 'rxjs';
import { TransactionCategory } from './transaction-category.model';
import { Update } from '@ngrx/entity';
import { mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TransactionCategoryService {
  http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  getAll(): Observable<TransactionCategory[]> {
    return this.http.get<TransactionCategory[]>(`/api/transaction-categories`);
  }

  getOne(key: string): Observable<TransactionCategory> {
    return this.http.get<TransactionCategory>(`/api/transaction-categories/${key}`);
  }

  create(transactionCategory: TransactionCategory): Observable<TransactionCategory> {
    return this.http.post<TransactionCategory>(`/api/transaction-categories`, transactionCategory);
  }

  delete(keys: string[]): Observable<any> {
    return forkJoin(keys.map(key => this.http.delete(`/api/transaction-categories/${key}`)));
  }

  update(update: Update<TransactionCategory>): Observable<any> {
    return this.http.put(`/api/transaction-categories/${update.id}`, update.changes);
  }
}
