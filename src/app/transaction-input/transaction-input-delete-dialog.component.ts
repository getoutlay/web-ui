import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { TransactionInput } from './transaction-input.model';

@Component({
  selector: 'app-transaction-input-delete-dialog',
  templateUrl: './transaction-input-delete-dialog.component.html',
  styleUrls: ['./transaction-input-delete-dialog.component.sass'],
})
export class TransactionInputDeleteDialogComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogInput,
    public dialogRef: MatDialogRef<TransactionInputDeleteDialogComponent>
  ) {}

  ngOnInit(): void {}
}

export class DialogInput {
  selection: TransactionInput[];
}
