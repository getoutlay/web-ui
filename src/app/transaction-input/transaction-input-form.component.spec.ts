import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TransactionInputFormComponent } from './transaction-input-form.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { RouterModule } from '@angular/router';

const createNewEvent = (eventName: string, bubbles = false, cancelable = false): CustomEvent => {
  const evt = document.createEvent('CustomEvent');
  evt.initCustomEvent(eventName, bubbles, cancelable, null);
  return evt;
};

describe('TransactionInputFormComponent', () => {
  let component: TransactionInputFormComponent;
  let fixture: ComponentFixture<TransactionInputFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers), RouterModule.forRoot([], { relativeLinkResolution: 'legacy' })],
      declarations: [TransactionInputFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionInputFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have content', () => {
    expect(component.content).toBeTruthy();
  });

  it('should have status', () => {
    expect(component.status).toBeTruthy();
  });

  it('should have accountId', () => {
    expect(component.accountId).toBeTruthy();
  });

  it('initially should be invalid', () => {
    expect(component.transactionInputForm.valid).toBeFalsy();
  });

  it('initially should be valid upon input', () => {
    const event = createNewEvent('input');

    component.accountId.setValue(42);

    const content = fixture.nativeElement.querySelector('#content');
    content.value = 'Malayan Tapir';
    content.dispatchEvent(event);

    component.status.setValue(0);

    const accountId = fixture.nativeElement.querySelector('#accountId');
    accountId.value = 17983;
    accountId.dispatchEvent(event);

    expect(component.transactionInputForm.valid).toBeTruthy();
  });
});
