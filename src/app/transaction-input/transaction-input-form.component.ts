import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, AbstractControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { State } from '../reducers';
import { transactionInputActionTypes } from './transaction-input.actions';
import { Observable } from 'rxjs';
import { TransactionInput } from './transaction-input.model';
import { Router } from '@angular/router';
import { accountActionTypes } from '../account/account.actions';
import { selectAllAccounts } from '../account/account.selectors';
import { Account } from '../account/account.model';

@Component({
  selector: 'app-transaction-input-form',
  templateUrl: './transaction-input-form.component.html',
  styleUrls: ['./transaction-input-form.component.sass'],
})
export class TransactionInputFormComponent implements OnInit {
  @Input() transactionInput: Observable<TransactionInput>;
  accounts$: Observable<Account[]>;

  transactionInputForm = new FormGroup({
    id: new FormControl(''),
    content: new FormControl(''),
    status: new FormControl(''),
    accountId: new FormControl(''),
  });

  constructor(private store: Store<State>, private router: Router) {}

  ngOnInit(): void {
    this.store.dispatch(accountActionTypes.loadAccounts());
    this.accounts$ = this.store.select(selectAllAccounts);
    if (this.transactionInput) {
      this.transactionInput.subscribe((transactionInput) => {
        if (transactionInput && transactionInput.id) {
          this.transactionInputForm.setValue(transactionInput);
        }
      });
    }
  }

  get content(): FormControl {
    return this.transactionInputForm.get('content') as FormControl;
  }

  get status(): FormControl {
    return this.transactionInputForm.get('status') as FormControl;
  }

  get accountId(): FormControl {
    return this.transactionInputForm.get('accountId') as FormControl;
  }

  fileInputChange(fileInputEvent: any): void {
    const filename = fileInputEvent.target.files[0];
    const reader = new FileReader();
    reader.onload = (e) => {
      const contents = e.target.result;
      this.content.setValue(contents);
    };
    reader.readAsDataURL(filename);
  }

  onSubmit(): void {
    if (this.transactionInputForm.valid) {
      if (this.transactionInputForm.value.id) {
        this.store.dispatch(
          transactionInputActionTypes.updateTransactionInput({
            update: {
              id: this.transactionInputForm.value.id,
              changes: this.transactionInputForm.value,
            },
          })
        );
        this.router.navigate(['/transaction-input']);
      } else {
        const value = this.transactionInputForm.value;
        const { id, ...transactionInput } = value;
        this.store.dispatch(transactionInputActionTypes.createTransactionInput({ transactionInput }));
      }
    }
  }
}
