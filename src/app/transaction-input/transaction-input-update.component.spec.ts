import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TransactionInputUpdateComponent } from './transaction-input-update.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { RouterModule } from '@angular/router';
import { TransactionInputFormComponent } from './transaction-input-form.component';

describe('TransactionInputUpdateComponent', () => {
  let component: TransactionInputUpdateComponent;
  let fixture: ComponentFixture<TransactionInputUpdateComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers), RouterModule.forRoot([], { relativeLinkResolution: 'legacy' })],
      declarations: [TransactionInputUpdateComponent, TransactionInputFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionInputUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
