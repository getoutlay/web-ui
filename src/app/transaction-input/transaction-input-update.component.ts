import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { TransactionInput } from './transaction-input.model';
import { Store, select } from '@ngrx/store';
import { State } from '../reducers';
import { selectCurrentTransactionInput } from './transaction-input.selectors';
import { transactionInputActionTypes } from './transaction-input.actions';

@Component({
  selector: 'app-transaction-input-update',
  templateUrl: './transaction-input-update.component.html',
  styleUrls: ['./transaction-input-update.component.sass'],
})
export class TransactionInputUpdateComponent implements OnInit {
  transactionInput$: Observable<TransactionInput>;

  constructor(private store: Store<State>, private route: ActivatedRoute) {
    this.route.params.pipe(map((params) => params.id)).subscribe((id) => {
      this.store.dispatch(transactionInputActionTypes.loadTransactionInput({ key: id }));
    });
    this.transactionInput$ = this.store.select(selectCurrentTransactionInput);
  }

  ngOnInit(): void {}
}
