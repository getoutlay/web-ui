import { createAction, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { TransactionInput } from './transaction-input.model';

export const loadTransactionInputs = createAction('[TransactionInput/API] Load Transaction Inputs');
export const transactionInputsLoaded = createAction('[TransactionInput/API] Transaction Inputs Loaded Successfully',
  props<{ transactionInputs: TransactionInput[] }>());

export const loadTransactionInput = createAction('[TransactionInput/API] Load TransactionInput', props<{ key: string }>());
export const transactionInputLoaded = createAction('[TransactionInput/API] TransactionInput Loaded Successfully',
  props<{ transactionInput: TransactionInput }>());

export const createTransactionInput = createAction('[TransactionInput/API] Create TransactionInput',
  props<{ transactionInput: TransactionInput }>());

export const deleteTransactionInputs = createAction('[TransactionInput/API] Delete Transaction Inputs',
  props<{ keys: string[] }>());

export const updateTransactionInput = createAction('[TransactionInput/API] Update TransactionInput',
  props<{ update: Update<TransactionInput> }>());

export const transactionInputActionTypes = {
  loadTransactionInputs,
  transactionInputsLoaded,
  loadTransactionInput,
  transactionInputLoaded,
  createTransactionInput,
  deleteTransactionInputs,
  updateTransactionInput,
};
