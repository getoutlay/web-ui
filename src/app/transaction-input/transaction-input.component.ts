import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { TransactionInput } from './transaction-input.model';
import { selectAllTransactionInputs } from './transaction-input.selectors';
import { State } from '../reducers';
import { transactionInputActionTypes } from './transaction-input.actions';
import * as uuid from 'uuid';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { TransactionInputDeleteDialogComponent } from './transaction-input-delete-dialog.component';

@Component({
  selector: 'app-transaction-input',
  templateUrl: './transaction-input.component.html',
  styleUrls: ['./transaction-input.component.sass'],
})
export class TransactionInputComponent implements OnInit {
  public dataSource: Observable<TransactionInput[]>;
  public displayedColumns: string[] = [
    'id',
    'delete',
    'content',
    'status',
    'accountId',
  ];
  public selection = new SelectionModel<TransactionInput>(true, []);

  constructor(private store: Store<State>, private dialog: MatDialog) {}

  ngOnInit(): void {
    this.store.dispatch(transactionInputActionTypes.loadTransactionInputs());
    this.dataSource = this.store.select(selectAllTransactionInputs);
  }

  onDeleteClick(event: Event): void {
    // prevent parent event happening (navigation)
    event.preventDefault();
    event.stopImmediatePropagation();

    const dialogRef = this.dialog.open(TransactionInputDeleteDialogComponent, {
      data: {
        selection: this.selection.selected,
      },
    });
    dialogRef.afterClosed().subscribe((result: TransactionInput[]) => {
      const keys = result.map((entity) => entity.id);
      if (keys && keys.length) {
        this.store.dispatch(transactionInputActionTypes.deleteTransactionInputs({ keys }));
        this.selection.clear();
      }
    });
  }

  toggleSelectAll(): void {
    if (this.selection.hasValue()) {
      this.selection.clear();
    } else {
      this.dataSource.forEach((rows) => rows.forEach((row) => this.selection.select(row)));
    }
  }

  create(entity: Partial<TransactionInput>): void {
    const transactionInput: TransactionInput = {
      id: uuid.v4(),
      content: entity.content,
      status: entity.status,
      accountId: entity.accountId,
    };
    this.store.dispatch(transactionInputActionTypes.createTransactionInput({ transactionInput }));
  }
}
