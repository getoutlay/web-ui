import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable, throwError, of } from 'rxjs';
import { cold } from 'jasmine-marbles';

import { TransactionInputEffects } from './transaction-input.effects';
import {
  loadTransactionInput,
  loadTransactionInputs,
  transactionInputLoaded,
  transactionInputsLoaded,
  createTransactionInput
} from './transaction-input.actions';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { SharedModule } from '../shared/shared.module';
import { Actions } from '@ngrx/effects';

const loadTransactionInputsStub = (response: any) => {
  const service = jasmine.createSpyObj('transactionInput', ['getAll']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.getAll.and.returnValue(serviceResponse);
  return service;
};

const loadTransactionInputStub = (response: any) => {
  const service = jasmine.createSpyObj('transactionInput', ['getOne']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.getOne.and.returnValue(serviceResponse);
  return service;
};

const createTransactionInputStub = (response: any) => {
  const service = jasmine.createSpyObj('transactionInput', ['create']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.create.and.returnValue(serviceResponse);
  return service;
};

describe('TransactionInputEffects', () => {
  // tslint:disable-next-line: prefer-const
  let actions$: Observable<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers)],
      providers: [TransactionInputEffects, provideMockActions(() => actions$)],
    });
  });

  it('should be created', () => {
    const effects = TestBed.inject(TransactionInputEffects);
    expect(effects).toBeTruthy();
  });

  it('loadTransactionInputs should return transactionInputsLoaded', () => {
    const source = cold('a', { a: loadTransactionInputs() });
    const service = loadTransactionInputsStub([]);
    const effects = new TransactionInputEffects(service, new Actions(source));

    const expected = cold('a', { a: transactionInputsLoaded({ transactionInputs: [] }) });
    expect(effects.loadTransactionInputs$).toBeObservable(expected);
  });

  it('loadTransactionInput should return transactionInputLoaded', () => {
    const source = cold('a', { a: loadTransactionInput({ key: '42' }) });
    const transactionInput = {
      id: '1',
      content: 'Malayan Tapir',
      status: 0,
      accountId: 17983,
    };
    const service = loadTransactionInputStub(transactionInput);
    const effects = new TransactionInputEffects(service, new Actions(source));

    const expected = cold('a', { a: transactionInputLoaded({ transactionInput }) });
    expect(effects.loadTransactionInput$).toBeObservable(expected);
  });

  it('createTransactionInput should return transactionInputLoaded', () => {
    const transactionInput = {
      id: '1',
      content: 'Malayan Tapir',
      status: 0,
      accountId: 17983,
    };
    const source = cold('a', { a: createTransactionInput({ transactionInput }) });
    const service = createTransactionInputStub(transactionInput);
    const effects = new TransactionInputEffects(service, new Actions(source));

    const expected = cold('a', { a: loadTransactionInputs() });
    expect(effects.createTransactionInput$).toBeObservable(expected);
  });
});
