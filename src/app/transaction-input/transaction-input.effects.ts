import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { TransactionInputService } from './transaction-input.service';
import { transactionInputActionTypes } from './transaction-input.actions';
import { concatMap, map } from 'rxjs/operators';

@Injectable()
export class TransactionInputEffects {
  constructor(private service: TransactionInputService, private actions$: Actions) {}

  loadTransactionInputs$ = createEffect(() =>
    this.actions$.pipe(
      ofType(transactionInputActionTypes.loadTransactionInputs),
      concatMap(() => this.service.getAll()),
      map((transactionInputs) => transactionInputActionTypes.transactionInputsLoaded({ transactionInputs }))
    )
  );

  loadTransactionInput$ = createEffect(() =>
    this.actions$.pipe(
      ofType(transactionInputActionTypes.loadTransactionInput),
      concatMap((action) => this.service.getOne(action.key)),
      map((transactionInput) => transactionInputActionTypes.transactionInputLoaded({ transactionInput }))
    )
  );

  createTransactionInput$ = createEffect(() =>
    this.actions$.pipe(
      ofType(transactionInputActionTypes.createTransactionInput),
      concatMap((action) => this.service.create(action.transactionInput)),
      map((transactionInput) => transactionInputActionTypes.loadTransactionInputs())
    )
  );

  updateTransactionInput$ = createEffect(() =>
    this.actions$.pipe(
      ofType(transactionInputActionTypes.updateTransactionInput),
      concatMap((action) => this.service.update(action.update)),
      map((transactionInput) => transactionInputActionTypes.loadTransactionInputs())
    )
  );

  deleteTransactionInputs$ = createEffect(() =>
    this.actions$.pipe(
      ofType(transactionInputActionTypes.deleteTransactionInputs),
      concatMap((action) => this.service.delete(action.keys)),
      map((transactionInput) => transactionInputActionTypes.loadTransactionInputs())
    )
  );
}
