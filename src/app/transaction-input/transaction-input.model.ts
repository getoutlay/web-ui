import { InputStatus } from '../input-status/input-status';
// mastic-pin-import-statements

export interface TransactionInput {
  id?: string;
  content: string;
  status: InputStatus;
  accountId: number;
}
