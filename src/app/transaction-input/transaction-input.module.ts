import { NgModule } from '@angular/core';
import { TransactionInputService } from './transaction-input.service';
import { StoreModule } from '@ngrx/store';
import { transactionInputsFeatureKey, transactionInputReducer } from './transaction-input.reducer';
import { HttpClientModule } from '@angular/common/http';
import { TransactionInputComponent } from './transaction-input.component';
import { TransactionInputFormComponent } from './transaction-input-form.component';
import { TransactionInputUpdateComponent } from './transaction-input-update.component';
import { SharedModule } from '../shared/shared.module';
import { TransactionInputDeleteDialogComponent } from './transaction-input-delete-dialog.component';


@NgModule({
  declarations: [
    TransactionInputComponent,
    TransactionInputFormComponent,
    TransactionInputUpdateComponent,
    TransactionInputDeleteDialogComponent
  ],
  imports: [
    SharedModule,
    HttpClientModule,
    StoreModule.forFeature(transactionInputsFeatureKey, transactionInputReducer),
  ],
  providers: [
    TransactionInputService,
    HttpClientModule,
  ],
})
export class TransactionInputModule { }
