import { transactionInputReducer, initialState } from './transaction-input.reducer';
import {
  createTransactionInput,
  updateTransactionInput,
  deleteTransactionInputs,
  transactionInputLoaded,
  transactionInputsLoaded
} from './transaction-input.actions';

describe('TransactionInput Reducer', () => {
  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;
      const result = transactionInputReducer(initialState, action);
      expect(result).toBe(initialState);
    });
  });

  describe('updateTransactionInput', () => {
    it('should not update missing one', () => {
      const action = updateTransactionInput({ update: { id: '1', changes: {
        content: 'Malayan Tapir Updated',
        status: 0,
        accountId: 18025,
      } } });
      const result = transactionInputReducer(initialState, action);
      expect(result).toEqual({ ids: [], entities: {} });
    });

    it('should update one', () => {
      const transactionInput = {
        id: '1',
        content: 'Malayan Tapir',
        status: 0,
        accountId: 17983,
      };
      const action = updateTransactionInput({ update: { id: '1', changes: {
        content: 'Malayan Tapir Updated',
        status: 0,
        accountId: 18025,
      } } });
      const result = transactionInputReducer({ ids: ['1'], entities: { ['1']: transactionInput } }, action);
      expect(result.ids).toEqual(['1']);
      expect(result.entities['1']).toEqual({
        id: '1',
        content: 'Malayan Tapir Updated',
        status: 0,
        accountId: 18025,
      });
    });
  });

  describe('deleteTransactionInputs', () => {
    it('should delete one', () => {
      const transactionInput = {
        id: '1',
        content: 'Malayan Tapir',
        status: 0,
        accountId: 17983,
      };
      const action = deleteTransactionInputs({ keys: ['1'] });
      const result = transactionInputReducer({ ids: ['1'], entities: { ['1']: transactionInput } }, action);
      expect(result.ids).toEqual([]);
    });
  });

  describe('transactionInputLoaded', () => {
    it('should set one', () => {
      const transactionInput = {
        id: '1',
        content: 'Malayan Tapir',
        status: 0,
        accountId: 17983,
      };
      const action = transactionInputLoaded({ transactionInput });
      const result = transactionInputReducer(initialState, action);
      expect(result.ids).toEqual(['1']);
    });
  });

  describe('transactionInputsLoaded', () => {
    it('should sets all', () => {
      const transactionInputs = [
        {
          id: '1',
          content: 'Malayan Tapir',
          status: 0,
          accountId: 17983,
        },
        {
          id: '2',
          content: 'Malayan Tapir',
          status: 0,
          accountId: 17983,
        }
      ];
      const action = transactionInputsLoaded({ transactionInputs });
      const result = transactionInputReducer(initialState, action);
      expect(result.ids).toEqual(['1', '2']);
    });
  });
});
