import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { TransactionInput } from './transaction-input.model';
import * as TransactionInputActions from './transaction-input.actions';

export const transactionInputsFeatureKey = 'transactionInputs';

export interface TransactionInputState extends EntityState<TransactionInput> {
  entity?: TransactionInput;
}

export const adapter: EntityAdapter<TransactionInput> = createEntityAdapter<TransactionInput>();

export const initialState: TransactionInputState = adapter.getInitialState({});

export const transactionInputReducer = createReducer(
  initialState,
  on(TransactionInputActions.updateTransactionInput, (state, action) => adapter.updateOne(action.update, state)),
  on(TransactionInputActions.deleteTransactionInputs, (state, action) => adapter.removeMany(action.keys, state)),
  on(TransactionInputActions.transactionInputLoaded, (state, action) => {
    const entity = action.transactionInput;
    return { ...adapter.setOne(entity, state), entity };
  }),
  on(TransactionInputActions.transactionInputsLoaded, (state, action) => adapter.setAll(action.transactionInputs, state))
);

export const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors();
