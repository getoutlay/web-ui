import { transactionInputFeatureSelector, selectAllTransactionInputs } from './transaction-input.selectors';
import { State } from '../reducers';

const state: State = {
  accounts: {
    ids: [],
    entities: {}
  },
  categories: {
    ids: [],
    entities: {}
  },
  transactions: {
    ids: [],
    entities: {}
  },
  transactionCategories: {
    ids: [],
    entities: {}
  },
  inputTypes: {
    ids: [],
    entities: {}
  },
  transactionInputs: {
    ids: ['1', '2', '3'],
    entities: {
      1: { id: '1',
           content: 'Malayan Tapir',
           status: 0,
           accountId: 17983,
      },
      2: { id: '2',
           content: 'Malayan Tapir',
           status: 0,
           accountId: 17983,
      },
      3: { id: '3',
           content: 'Malayan Tapir',
           status: 0,
           accountId: 17983,
      },
    },
  },
};

describe('TransactionInput Selectors', () => {
  it('transactionInputFeatureSelector', () => {
    expect(transactionInputFeatureSelector.projector(state.transactionInputs)).toEqual(state.transactionInputs);
  });

  it('getAllTransactionInputs', () => {
    expect(selectAllTransactionInputs.projector(state.transactionInputs)).toEqual([
      state.transactionInputs.entities[1],
      state.transactionInputs.entities[2],
      state.transactionInputs.entities[3],
    ]);
  });
});
