import { createFeatureSelector, createSelector } from '@ngrx/store';
import { TransactionInputState, transactionInputsFeatureKey, selectAll, selectEntities } from './transaction-input.reducer';

export const transactionInputFeatureSelector = createFeatureSelector<TransactionInputState>(transactionInputsFeatureKey);
export const selectAllTransactionInputs = createSelector(transactionInputFeatureSelector, selectAll);
export const selectCurrentTransactionInput = createSelector(transactionInputFeatureSelector,
  (transactionInputState) => transactionInputState.entity);
