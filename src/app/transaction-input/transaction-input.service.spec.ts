import { TestBed } from '@angular/core/testing';

import { TransactionInputService } from './transaction-input.service';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { SharedModule } from '../shared/shared.module';

describe('TransactionInputService', () => {
  let service: TransactionInputService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers)],
    });
    service = TestBed.inject(TransactionInputService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
