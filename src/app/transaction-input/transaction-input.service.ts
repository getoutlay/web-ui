import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, pipe, forkJoin } from 'rxjs';
import { TransactionInput } from './transaction-input.model';
import { Update } from '@ngrx/entity';
import { mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TransactionInputService {
  http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  getAll(): Observable<TransactionInput[]> {
    return this.http.get<TransactionInput[]>(`/api/transaction-inputs`);
  }

  getOne(key: string): Observable<TransactionInput> {
    return this.http.get<TransactionInput>(`/api/transaction-inputs/${key}`);
  }

  create(transactionInput: TransactionInput): Observable<TransactionInput> {
    return this.http.post<TransactionInput>(`/api/transaction-inputs`, transactionInput);
  }

  delete(keys: string[]): Observable<any> {
    return forkJoin(keys.map(key => this.http.delete(`/api/transaction-inputs/${key}`)));
  }

  update(update: Update<TransactionInput>): Observable<any> {
    return this.http.put(`/api/transaction-inputs/${update.id}`, update.changes);
  }
}
