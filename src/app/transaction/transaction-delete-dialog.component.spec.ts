import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TransactionDeleteDialogComponent } from './transaction-delete-dialog.component';
import { SharedModule } from '../shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

describe('TransactionDeleteDialogComponent', () => {
  let component: TransactionDeleteDialogComponent;
  let fixture: ComponentFixture<TransactionDeleteDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TransactionDeleteDialogComponent],
      imports: [SharedModule, BrowserAnimationsModule],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            selection: [{
              id: '1',
              desription: 'Malayan Tapir',
              amount: 87934588315.2384,
              currency: 0,
              type: 'Malayan Tapir',
              bookedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
              executedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
              explanation: 'Malayan Tapir',
              reference: 'Malayan Tapir',
              corresponderName: 'Malayan Tapir',
              corresponderBank: 'Malayan Tapir',
              corresponderAccount: 'Malayan Tapir',
            }],
          },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionDeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have data', () => {
    expect(component.data).toBeTruthy();
    expect(component.data.selection).toBeTruthy();
    expect(component.data.selection.length).toEqual(1);
  });

});
