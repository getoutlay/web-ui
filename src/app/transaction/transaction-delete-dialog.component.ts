import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Transaction } from './transaction.model';

@Component({
  selector: 'app-transaction-delete-dialog',
  templateUrl: './transaction-delete-dialog.component.html',
  styleUrls: ['./transaction-delete-dialog.component.sass'],
})
export class TransactionDeleteDialogComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogInput,
    public dialogRef: MatDialogRef<TransactionDeleteDialogComponent>
  ) {}

  ngOnInit(): void {}
}

export class DialogInput {
  selection: Transaction[];
}
