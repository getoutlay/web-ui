import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TransactionFormComponent } from './transaction-form.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { RouterModule } from '@angular/router';

const createNewEvent = (eventName: string, bubbles = false, cancelable = false): CustomEvent => {
  const evt = document.createEvent('CustomEvent');
  evt.initCustomEvent(eventName, bubbles, cancelable, null);
  return evt;
};

describe('TransactionFormComponent', () => {
  let component: TransactionFormComponent;
  let fixture: ComponentFixture<TransactionFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers), RouterModule.forRoot([], { relativeLinkResolution: 'legacy' })],
      declarations: [TransactionFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have desription', () => {
    expect(component.desription).toBeTruthy();
  });

  it('should have amount', () => {
    expect(component.amount).toBeTruthy();
  });

  it('should have currency', () => {
    expect(component.currency).toBeTruthy();
  });

  it('should have type', () => {
    expect(component.type).toBeTruthy();
  });

  it('should have bookedOn', () => {
    expect(component.bookedOn).toBeTruthy();
  });

  it('should have executedOn', () => {
    expect(component.executedOn).toBeTruthy();
  });

  it('should have explanation', () => {
    expect(component.explanation).toBeTruthy();
  });

  it('should have reference', () => {
    expect(component.reference).toBeTruthy();
  });

  it('should have corresponderName', () => {
    expect(component.corresponderName).toBeTruthy();
  });

  it('should have corresponderBank', () => {
    expect(component.corresponderBank).toBeTruthy();
  });

  it('should have corresponderAccount', () => {
    expect(component.corresponderAccount).toBeTruthy();
  });

  it('initially should be invalid', () => {
    expect(component.transactionForm.valid).toBeFalsy();
  });

  it('initially should be valid upon input', () => {
    const event = createNewEvent('input');

    const desription = fixture.nativeElement.querySelector('#desription');
    desription.value = 'Malayan Tapir';
    desription.dispatchEvent(event);

    const amount = fixture.nativeElement.querySelector('#amount');
    amount.value = 87934588315.2384;
    amount.dispatchEvent(event);

    component.currency.setValue(0);

    const type = fixture.nativeElement.querySelector('#type');
    type.value = 'Malayan Tapir';
    type.dispatchEvent(event);

    const bookedOn = fixture.nativeElement.querySelector('#bookedOn');
    bookedOn.value = new Date(Date.UTC(2020, 1, 0, 9, 0)).toISOString().split('T')[0];
    bookedOn.dispatchEvent(event);

    const executedOn = fixture.nativeElement.querySelector('#executedOn');
    executedOn.value = new Date(Date.UTC(2020, 1, 0, 9, 0)).toISOString().split('T')[0];
    executedOn.dispatchEvent(event);

    const explanation = fixture.nativeElement.querySelector('#explanation');
    explanation.value = 'Malayan Tapir';
    explanation.dispatchEvent(event);

    const reference = fixture.nativeElement.querySelector('#reference');
    reference.value = 'Malayan Tapir';
    reference.dispatchEvent(event);

    const corresponderName = fixture.nativeElement.querySelector('#corresponderName');
    corresponderName.value = 'Malayan Tapir';
    corresponderName.dispatchEvent(event);

    const corresponderBank = fixture.nativeElement.querySelector('#corresponderBank');
    corresponderBank.value = 'Malayan Tapir';
    corresponderBank.dispatchEvent(event);

    const corresponderAccount = fixture.nativeElement.querySelector('#corresponderAccount');
    corresponderAccount.value = 'Malayan Tapir';
    corresponderAccount.dispatchEvent(event);

    expect(component.transactionForm.valid).toBeTruthy();
  });
});
