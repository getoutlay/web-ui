import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, AbstractControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { State } from '../reducers';
import { transactionActionTypes } from './transaction.actions';
import { Observable } from 'rxjs';
import { Transaction } from './transaction.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-transaction-form',
  templateUrl: './transaction-form.component.html',
  styleUrls: ['./transaction-form.component.sass'],
})
export class TransactionFormComponent implements OnInit {
  @Input() transaction: Observable<Transaction>;

  transactionForm = new FormGroup({
    id: new FormControl(''),
    desription: new FormControl(''),
    amount: new FormControl(''),
    currency: new FormControl(''),
    type: new FormControl(''),
    bookedOn: new FormControl(''),
    executedOn: new FormControl(''),
    explanation: new FormControl(''),
    reference: new FormControl(''),
    corresponderName: new FormControl(''),
    corresponderBank: new FormControl(''),
    corresponderAccount: new FormControl(''),
  });

  constructor(private store: Store<State>, private router: Router) {}

  ngOnInit(): void {
    if (this.transaction) {
      this.transaction.subscribe((transaction) => {
        if (transaction && transaction.id) {
          this.transactionForm.setValue(transaction);
        }
      });
    }
  }

  get desription(): FormControl {
    return this.transactionForm.get('desription') as FormControl;
  }

  get amount(): FormControl {
    return this.transactionForm.get('amount') as FormControl;
  }

  get currency(): FormControl {
    return this.transactionForm.get('currency') as FormControl;
  }

  get type(): FormControl {
    return this.transactionForm.get('type') as FormControl;
  }

  get bookedOn(): FormControl {
    return this.transactionForm.get('bookedOn') as FormControl;
  }

  get executedOn(): FormControl {
    return this.transactionForm.get('executedOn') as FormControl;
  }

  get explanation(): FormControl {
    return this.transactionForm.get('explanation') as FormControl;
  }

  get reference(): FormControl {
    return this.transactionForm.get('reference') as FormControl;
  }

  get corresponderName(): FormControl {
    return this.transactionForm.get('corresponderName') as FormControl;
  }

  get corresponderBank(): FormControl {
    return this.transactionForm.get('corresponderBank') as FormControl;
  }

  get corresponderAccount(): FormControl {
    return this.transactionForm.get('corresponderAccount') as FormControl;
  }

  onSubmit(): void {
    if (this.transactionForm.valid) {
      if (this.transactionForm.value.id) {
        this.store.dispatch(
          transactionActionTypes.updateTransaction({
            update: {
              id: this.transactionForm.value.id,
              changes: this.transactionForm.value,
            },
          })
        );
        this.router.navigate(['/transaction']);
      } else {
        const value = this.transactionForm.value;
        const { id, ...transaction } = value;
        this.store.dispatch(transactionActionTypes.createTransaction({ transaction }));
      }
    }
  }
}
