import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TransactionUpdateComponent } from './transaction-update.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { RouterModule } from '@angular/router';
import { TransactionFormComponent } from './transaction-form.component';

describe('TransactionUpdateComponent', () => {
  let component: TransactionUpdateComponent;
  let fixture: ComponentFixture<TransactionUpdateComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers), RouterModule.forRoot([], { relativeLinkResolution: 'legacy' })],
      declarations: [TransactionUpdateComponent, TransactionFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
