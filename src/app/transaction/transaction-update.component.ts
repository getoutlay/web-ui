import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Transaction } from './transaction.model';
import { Store, select } from '@ngrx/store';
import { State } from '../reducers';
import { selectCurrentTransaction } from './transaction.selectors';
import { transactionActionTypes } from './transaction.actions';

@Component({
  selector: 'app-transaction-update',
  templateUrl: './transaction-update.component.html',
  styleUrls: ['./transaction-update.component.sass'],
})
export class TransactionUpdateComponent implements OnInit {
  transaction$: Observable<Transaction>;

  constructor(private store: Store<State>, private route: ActivatedRoute) {
    this.route.params.pipe(map((params) => params.id)).subscribe((id) => {
      this.store.dispatch(transactionActionTypes.loadTransaction({ key: id }));
    });
    this.transaction$ = this.store.select(selectCurrentTransaction);
  }

  ngOnInit(): void {}
}
