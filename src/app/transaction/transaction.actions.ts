import { createAction, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { Transaction } from './transaction.model';

export const loadTransactions = createAction('[Transaction/API] Load Transactions');
export const transactionsLoaded = createAction('[Transaction/API] Transactions Loaded Successfully',
  props<{ transactions: Transaction[] }>());

export const loadTransaction = createAction('[Transaction/API] Load Transaction', props<{ key: string }>());
export const transactionLoaded = createAction('[Transaction/API] Transaction Loaded Successfully',
  props<{ transaction: Transaction }>());

export const createTransaction = createAction('[Transaction/API] Create Transaction',
  props<{ transaction: Transaction }>());

export const deleteTransactions = createAction('[Transaction/API] Delete Transactions',
  props<{ keys: string[] }>());

export const updateTransaction = createAction('[Transaction/API] Update Transaction',
  props<{ update: Update<Transaction> }>());

export const transactionActionTypes = {
  loadTransactions,
  transactionsLoaded,
  loadTransaction,
  transactionLoaded,
  createTransaction,
  deleteTransactions,
  updateTransaction,
};
