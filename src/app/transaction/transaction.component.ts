import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Transaction } from './transaction.model';
import { selectAllTransactions } from './transaction.selectors';
import { State } from '../reducers';
import { transactionActionTypes } from './transaction.actions';
import * as uuid from 'uuid';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { TransactionDeleteDialogComponent } from './transaction-delete-dialog.component';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.sass'],
})
export class TransactionComponent implements OnInit {
  public dataSource: Observable<Transaction[]>;
  public displayedColumns: string[] = [
    'id',
    'delete',
    'desription',
    'amount',
    'currency',
    'type',
    'bookedOn',
    'executedOn',
    'explanation',
    'reference',
    'corresponderName',
    'corresponderBank',
    'corresponderAccount',
  ];
  public selection = new SelectionModel<Transaction>(true, []);

  constructor(private store: Store<State>, private dialog: MatDialog) {}

  ngOnInit(): void {
    this.store.dispatch(transactionActionTypes.loadTransactions());
    this.dataSource = this.store.select(selectAllTransactions);
  }

  onDeleteClick(event: Event): void {
    // prevent parent event happening (navigation)
    event.preventDefault();
    event.stopImmediatePropagation();

    const dialogRef = this.dialog.open(TransactionDeleteDialogComponent, {
      data: {
        selection: this.selection.selected,
      },
    });
    dialogRef.afterClosed().subscribe((result: Transaction[]) => {
      const keys = result.map((entity) => entity.id);
      if (keys && keys.length) {
        this.store.dispatch(transactionActionTypes.deleteTransactions({ keys }));
        this.selection.clear();
      }
    });
  }

  toggleSelectAll(): void {
    if (this.selection.hasValue()) {
      this.selection.clear();
    } else {
      this.dataSource.forEach((rows) => rows.forEach((row) => this.selection.select(row)));
    }
  }

  create(entity: Partial<Transaction>): void {
    const transaction: Transaction = {
      id: uuid.v4(),
      desription: entity.desription,
      amount: entity.amount,
      currency: entity.currency,
      type: entity.type,
      bookedOn: entity.bookedOn,
      executedOn: entity.executedOn,
      explanation: entity.explanation,
      reference: entity.reference,
      corresponderName: entity.corresponderName,
      corresponderBank: entity.corresponderBank,
      corresponderAccount: entity.corresponderAccount,
    };
    this.store.dispatch(transactionActionTypes.createTransaction({ transaction }));
  }
}
