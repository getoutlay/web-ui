import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable, throwError, of } from 'rxjs';
import { cold } from 'jasmine-marbles';

import { TransactionEffects } from './transaction.effects';
import {
  loadTransaction,
  loadTransactions,
  transactionLoaded,
  transactionsLoaded,
  createTransaction
} from './transaction.actions';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { SharedModule } from '../shared/shared.module';
import { Actions } from '@ngrx/effects';

const loadTransactionsStub = (response: any) => {
  const service = jasmine.createSpyObj('transaction', ['getAll']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.getAll.and.returnValue(serviceResponse);
  return service;
};

const loadTransactionStub = (response: any) => {
  const service = jasmine.createSpyObj('transaction', ['getOne']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.getOne.and.returnValue(serviceResponse);
  return service;
};

const createTransactionStub = (response: any) => {
  const service = jasmine.createSpyObj('transaction', ['create']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.create.and.returnValue(serviceResponse);
  return service;
};

describe('TransactionEffects', () => {
  // tslint:disable-next-line: prefer-const
  let actions$: Observable<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers)],
      providers: [TransactionEffects, provideMockActions(() => actions$)],
    });
  });

  it('should be created', () => {
    const effects = TestBed.inject(TransactionEffects);
    expect(effects).toBeTruthy();
  });

  it('loadTransactions should return transactionsLoaded', () => {
    const source = cold('a', { a: loadTransactions() });
    const service = loadTransactionsStub([]);
    const effects = new TransactionEffects(service, new Actions(source));

    const expected = cold('a', { a: transactionsLoaded({ transactions: [] }) });
    expect(effects.loadTransactions$).toBeObservable(expected);
  });

  it('loadTransaction should return transactionLoaded', () => {
    const source = cold('a', { a: loadTransaction({ key: '42' }) });
    const transaction = {
      id: '1',
      desription: 'Malayan Tapir',
      amount: 87934588315.2384,
      currency: 0,
      type: 'Malayan Tapir',
      bookedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
      executedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
      explanation: 'Malayan Tapir',
      reference: 'Malayan Tapir',
      corresponderName: 'Malayan Tapir',
      corresponderBank: 'Malayan Tapir',
      corresponderAccount: 'Malayan Tapir',
    };
    const service = loadTransactionStub(transaction);
    const effects = new TransactionEffects(service, new Actions(source));

    const expected = cold('a', { a: transactionLoaded({ transaction }) });
    expect(effects.loadTransaction$).toBeObservable(expected);
  });

  it('createTransaction should return transactionLoaded', () => {
    const transaction = {
      id: '1',
      desription: 'Malayan Tapir',
      amount: 87934588315.2384,
      currency: 0,
      type: 'Malayan Tapir',
      bookedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
      executedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
      explanation: 'Malayan Tapir',
      reference: 'Malayan Tapir',
      corresponderName: 'Malayan Tapir',
      corresponderBank: 'Malayan Tapir',
      corresponderAccount: 'Malayan Tapir',
    };
    const source = cold('a', { a: createTransaction({ transaction }) });
    const service = createTransactionStub(transaction);
    const effects = new TransactionEffects(service, new Actions(source));

    const expected = cold('a', { a: loadTransactions() });
    expect(effects.createTransaction$).toBeObservable(expected);
  });
});
