import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { TransactionService } from './transaction.service';
import { transactionActionTypes } from './transaction.actions';
import { concatMap, map } from 'rxjs/operators';

@Injectable()
export class TransactionEffects {
  constructor(private service: TransactionService, private actions$: Actions) {}

  loadTransactions$ = createEffect(() =>
    this.actions$.pipe(
      ofType(transactionActionTypes.loadTransactions),
      concatMap(() => this.service.getAll()),
      map((transactions) => transactionActionTypes.transactionsLoaded({ transactions }))
    )
  );

  loadTransaction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(transactionActionTypes.loadTransaction),
      concatMap((action) => this.service.getOne(action.key)),
      map((transaction) => transactionActionTypes.transactionLoaded({ transaction }))
    )
  );

  createTransaction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(transactionActionTypes.createTransaction),
      concatMap((action) => this.service.create(action.transaction)),
      map((transaction) => transactionActionTypes.loadTransactions())
    )
  );

  updateTransaction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(transactionActionTypes.updateTransaction),
      concatMap((action) => this.service.update(action.update)),
      map((transaction) => transactionActionTypes.loadTransactions())
    )
  );

  deleteTransactions$ = createEffect(() =>
    this.actions$.pipe(
      ofType(transactionActionTypes.deleteTransactions),
      concatMap((action) => this.service.delete(action.keys)),
      map((transaction) => transactionActionTypes.loadTransactions())
    )
  );
}
