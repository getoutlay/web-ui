import { Currency } from '../currency/currency';
// mastic-pin-import-statements

export interface Transaction {
  id?: string;
  desription: string;
  amount: number;
  currency: Currency;
  type: string;
  bookedOn: Date;
  executedOn: Date;
  explanation: string;
  reference: string;
  corresponderName: string;
  corresponderBank: string;
  corresponderAccount: string;
}
