import { NgModule } from '@angular/core';
import { TransactionService } from './transaction.service';
import { StoreModule } from '@ngrx/store';
import { transactionsFeatureKey, transactionReducer } from './transaction.reducer';
import { HttpClientModule } from '@angular/common/http';
import { TransactionComponent } from './transaction.component';
import { TransactionFormComponent } from './transaction-form.component';
import { TransactionUpdateComponent } from './transaction-update.component';
import { SharedModule } from '../shared/shared.module';
import { TransactionDeleteDialogComponent } from './transaction-delete-dialog.component';


@NgModule({
  declarations: [
    TransactionComponent,
    TransactionFormComponent,
    TransactionUpdateComponent,
    TransactionDeleteDialogComponent
  ],
  imports: [
    SharedModule,
    HttpClientModule,
    StoreModule.forFeature(transactionsFeatureKey, transactionReducer),
  ],
  providers: [
    TransactionService,
    HttpClientModule,
  ],
})
export class TransactionModule { }
