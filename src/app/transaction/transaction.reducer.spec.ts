import { transactionReducer, initialState } from './transaction.reducer';
import {
  createTransaction,
  updateTransaction,
  deleteTransactions,
  transactionLoaded,
  transactionsLoaded
} from './transaction.actions';

describe('Transaction Reducer', () => {
  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;
      const result = transactionReducer(initialState, action);
      expect(result).toBe(initialState);
    });
  });

  describe('updateTransaction', () => {
    it('should not update missing one', () => {
      const action = updateTransaction({ update: { id: '1', changes: {
        desription: 'Malayan Tapir Updated',
        amount: 87934588357.2384,
        currency: 0,
        type: 'Malayan Tapir Updated',
        bookedOn: new Date(Date.UTC(2020, 1, 1, 9, 0)),
        executedOn: new Date(Date.UTC(2020, 1, 1, 9, 0)),
        explanation: 'Malayan Tapir Updated',
        reference: 'Malayan Tapir Updated',
        corresponderName: 'Malayan Tapir Updated',
        corresponderBank: 'Malayan Tapir Updated',
        corresponderAccount: 'Malayan Tapir Updated',
      } } });
      const result = transactionReducer(initialState, action);
      expect(result).toEqual({ ids: [], entities: {} });
    });

    it('should update one', () => {
      const transaction = {
        id: '1',
        desription: 'Malayan Tapir',
        amount: 87934588315.2384,
        currency: 0,
        type: 'Malayan Tapir',
        bookedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
        executedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
        explanation: 'Malayan Tapir',
        reference: 'Malayan Tapir',
        corresponderName: 'Malayan Tapir',
        corresponderBank: 'Malayan Tapir',
        corresponderAccount: 'Malayan Tapir',
      };
      const action = updateTransaction({ update: { id: '1', changes: {
        desription: 'Malayan Tapir Updated',
        amount: 87934588357.2384,
        currency: 0,
        type: 'Malayan Tapir Updated',
        bookedOn: new Date(Date.UTC(2020, 1, 1, 9, 0)),
        executedOn: new Date(Date.UTC(2020, 1, 1, 9, 0)),
        explanation: 'Malayan Tapir Updated',
        reference: 'Malayan Tapir Updated',
        corresponderName: 'Malayan Tapir Updated',
        corresponderBank: 'Malayan Tapir Updated',
        corresponderAccount: 'Malayan Tapir Updated',
      } } });
      const result = transactionReducer({ ids: ['1'], entities: { ['1']: transaction } }, action);
      expect(result.ids).toEqual(['1']);
      expect(result.entities['1']).toEqual({
        id: '1',
        desription: 'Malayan Tapir Updated',
        amount: 87934588357.2384,
        currency: 0,
        type: 'Malayan Tapir Updated',
        bookedOn: new Date(Date.UTC(2020, 1, 1, 9, 0)),
        executedOn: new Date(Date.UTC(2020, 1, 1, 9, 0)),
        explanation: 'Malayan Tapir Updated',
        reference: 'Malayan Tapir Updated',
        corresponderName: 'Malayan Tapir Updated',
        corresponderBank: 'Malayan Tapir Updated',
        corresponderAccount: 'Malayan Tapir Updated',
      });
    });
  });

  describe('deleteTransactions', () => {
    it('should delete one', () => {
      const transaction = {
        id: '1',
        desription: 'Malayan Tapir',
        amount: 87934588315.2384,
        currency: 0,
        type: 'Malayan Tapir',
        bookedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
        executedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
        explanation: 'Malayan Tapir',
        reference: 'Malayan Tapir',
        corresponderName: 'Malayan Tapir',
        corresponderBank: 'Malayan Tapir',
        corresponderAccount: 'Malayan Tapir',
      };
      const action = deleteTransactions({ keys: ['1'] });
      const result = transactionReducer({ ids: ['1'], entities: { ['1']: transaction } }, action);
      expect(result.ids).toEqual([]);
    });
  });

  describe('transactionLoaded', () => {
    it('should set one', () => {
      const transaction = {
        id: '1',
        desription: 'Malayan Tapir',
        amount: 87934588315.2384,
        currency: 0,
        type: 'Malayan Tapir',
        bookedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
        executedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
        explanation: 'Malayan Tapir',
        reference: 'Malayan Tapir',
        corresponderName: 'Malayan Tapir',
        corresponderBank: 'Malayan Tapir',
        corresponderAccount: 'Malayan Tapir',
      };
      const action = transactionLoaded({ transaction });
      const result = transactionReducer(initialState, action);
      expect(result.ids).toEqual(['1']);
    });
  });

  describe('transactionsLoaded', () => {
    it('should sets all', () => {
      const transactions = [
        {
          id: '1',
          desription: 'Malayan Tapir',
          amount: 87934588315.2384,
          currency: 0,
          type: 'Malayan Tapir',
          bookedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
          executedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
          explanation: 'Malayan Tapir',
          reference: 'Malayan Tapir',
          corresponderName: 'Malayan Tapir',
          corresponderBank: 'Malayan Tapir',
          corresponderAccount: 'Malayan Tapir',
        },
        {
          id: '2',
          desription: 'Malayan Tapir',
          amount: 87934588315.2384,
          currency: 0,
          type: 'Malayan Tapir',
          bookedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
          executedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
          explanation: 'Malayan Tapir',
          reference: 'Malayan Tapir',
          corresponderName: 'Malayan Tapir',
          corresponderBank: 'Malayan Tapir',
          corresponderAccount: 'Malayan Tapir',
        }
      ];
      const action = transactionsLoaded({ transactions });
      const result = transactionReducer(initialState, action);
      expect(result.ids).toEqual(['1', '2']);
    });
  });
});
