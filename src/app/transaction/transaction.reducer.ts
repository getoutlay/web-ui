import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Transaction } from './transaction.model';
import * as TransactionActions from './transaction.actions';

export const transactionsFeatureKey = 'transactions';

export interface TransactionState extends EntityState<Transaction> {
  entity?: Transaction;
}

export const adapter: EntityAdapter<Transaction> = createEntityAdapter<Transaction>();

export const initialState: TransactionState = adapter.getInitialState({});

export const transactionReducer = createReducer(
  initialState,
  on(TransactionActions.updateTransaction, (state, action) => adapter.updateOne(action.update, state)),
  on(TransactionActions.deleteTransactions, (state, action) => adapter.removeMany(action.keys, state)),
  on(TransactionActions.transactionLoaded, (state, action) => {
    const entity = action.transaction;
    return { ...adapter.setOne(entity, state), entity };
  }),
  on(TransactionActions.transactionsLoaded, (state, action) => adapter.setAll(action.transactions, state))
);

export const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors();
