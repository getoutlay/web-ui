import { transactionFeatureSelector, selectAllTransactions } from './transaction.selectors';
import { State } from '../reducers';

const state: State = {
  accounts: {
    ids: [],
    entities: {}
  },
  categories: {
    ids: [],
    entities: {}
  },
  transactionCategories: {
    ids: [],
    entities: {}
  },
  inputTypes: {
    ids: [],
    entities: {}
  },
  transactionInputs: {
    ids: [],
    entities: {}
  },
  transactions: {
    ids: ['1', '2', '3'],
    entities: {
      1: { id: '1',
           desription: 'Malayan Tapir',
           amount: 87934588315.2384,
           currency: 0,
           type: 'Malayan Tapir',
           bookedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
           executedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
           explanation: 'Malayan Tapir',
           reference: 'Malayan Tapir',
           corresponderName: 'Malayan Tapir',
           corresponderBank: 'Malayan Tapir',
           corresponderAccount: 'Malayan Tapir',
      },
      2: { id: '2',
           desription: 'Malayan Tapir',
           amount: 87934588315.2384,
           currency: 0,
           type: 'Malayan Tapir',
           bookedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
           executedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
           explanation: 'Malayan Tapir',
           reference: 'Malayan Tapir',
           corresponderName: 'Malayan Tapir',
           corresponderBank: 'Malayan Tapir',
           corresponderAccount: 'Malayan Tapir',
      },
      3: { id: '3',
           desription: 'Malayan Tapir',
           amount: 87934588315.2384,
           currency: 0,
           type: 'Malayan Tapir',
           bookedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
           executedOn: new Date(Date.UTC(2020, 1, 0, 9, 0)),
           explanation: 'Malayan Tapir',
           reference: 'Malayan Tapir',
           corresponderName: 'Malayan Tapir',
           corresponderBank: 'Malayan Tapir',
           corresponderAccount: 'Malayan Tapir',
      },
    },
  },
};

describe('Transaction Selectors', () => {
  it('transactionFeatureSelector', () => {
    expect(transactionFeatureSelector.projector(state.transactions)).toEqual(state.transactions);
  });

  it('getAllTransactions', () => {
    expect(selectAllTransactions.projector(state.transactions)).toEqual([
      state.transactions.entities[1],
      state.transactions.entities[2],
      state.transactions.entities[3],
    ]);
  });
});
