import { createFeatureSelector, createSelector } from '@ngrx/store';
import { TransactionState, transactionsFeatureKey, selectAll, selectEntities } from './transaction.reducer';

export const transactionFeatureSelector = createFeatureSelector<TransactionState>(transactionsFeatureKey);
export const selectAllTransactions = createSelector(transactionFeatureSelector, selectAll);
export const selectCurrentTransaction = createSelector(transactionFeatureSelector,
  (transactionState) => transactionState.entity);
