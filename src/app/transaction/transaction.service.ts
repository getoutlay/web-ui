import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, pipe, forkJoin } from 'rxjs';
import { Transaction } from './transaction.model';
import { Update } from '@ngrx/entity';
import { mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TransactionService {
  http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  getAll(): Observable<Transaction[]> {
    return this.http.get<Transaction[]>(`/api/transactions`);
  }

  getOne(key: string): Observable<Transaction> {
    return this.http.get<Transaction>(`/api/transactions/${key}`);
  }

  create(transaction: Transaction): Observable<Transaction> {
    return this.http.post<Transaction>(`/api/transactions`, transaction);
  }

  delete(keys: string[]): Observable<any> {
    return forkJoin(keys.map(key => this.http.delete(`/api/transactions/${key}`)));
  }

  update(update: Update<Transaction>): Observable<any> {
    return this.http.put(`/api/transactions/${update.id}`, update.changes);
  }
}
