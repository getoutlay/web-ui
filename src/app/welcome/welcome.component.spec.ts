import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { WelcomeComponent } from './welcome.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';

describe('WelcomeComponent', () => {
  let component: WelcomeComponent;
  let fixture: ComponentFixture<WelcomeComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, RouterModule.forRoot([], { relativeLinkResolution: 'legacy' })],
      declarations: [WelcomeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
